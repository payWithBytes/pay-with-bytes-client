package com.paywithbytes.peer.db.repository.test;

import com.paywithbytes.peer.db.repository.PeerChunkDataRepository;
import com.paywithbytes.peer.db.repository.PeerChunkDataSQLiteRepository;

/**
 * This initializes the actual SQLite Repository for the base test.
 * 
 */
public class PeerChunkDataSQLiteRepositoryTest extends
		PeerChunkDataRepositoryTestBase<PeerChunkDataRepository> {

	@Override
	protected PeerChunkDataRepository createRepositoryInstance() {
		return new PeerChunkDataSQLiteRepository();
	}

}
