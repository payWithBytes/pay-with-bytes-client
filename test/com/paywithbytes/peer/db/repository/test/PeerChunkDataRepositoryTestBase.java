package com.paywithbytes.peer.db.repository.test;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Random;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.peer.db.repository.PeerChunkDataRepository;
import com.paywithbytes.persistency.beans.PeerChunkData;
import com.paywithbytes.persistency.beans.PeerChunkMetadata;
import com.paywithbytes.persistency.beans.Types;

/**
 * Test the Repository interface. This is just the Base, not the actual test.
 * 
 * @param <T>
 */
public abstract class PeerChunkDataRepositoryTestBase<T extends PeerChunkDataRepository> {

	private T repository;

	protected abstract T createRepositoryInstance();

	@Before
	public void setUp() {
		repository = createRepositoryInstance();
	}

	@Test
	public void testInsertAndSelect() throws NoSuchAlgorithmException, NoSuchProviderException, RepositoryException {
		Random rand = new Random();
		byte[] data = new byte[2048];
		rand.nextBytes(data);

		// Create object to insert
		PeerChunkData fileMetadata = new PeerChunkData(new PeerChunkMetadata(UUID.randomUUID().toString()
				+ rand.nextInt(), 2048, "30 - 1912", "-1882", Types.Private), new Chunk(data, rand.nextInt()));

		// Insert
		repository.insert(fileMetadata);

		// Retrieve
		PeerChunkData retrievedItem = repository.select(fileMetadata.getPeerChunkMetadata().getFragmentUUID());

		// Compare
		Assert.assertTrue(fileMetadata.equals(retrievedItem));
	}

	@Test
	public void testInsertAndDeleteAndSelect() throws NoSuchAlgorithmException, NoSuchProviderException,
			RepositoryException {

		Random rand = new Random();
		byte[] data = new byte[2048];
		rand.nextBytes(data);

		// Create object to insert
		PeerChunkData fileData = new PeerChunkData(new PeerChunkMetadata(UUID.randomUUID().toString()
				+ rand.nextInt(), 2048, "30 - 1912", "-1882", Types.Private), new Chunk(data, rand.nextInt()));

		// Insert
		repository.insert(fileData);

		// Retrieve
		PeerChunkData retrievedItem = repository.select(fileData.getPeerChunkMetadata().getFragmentUUID());

		// Compare
		Assert.assertTrue(fileData.equals(retrievedItem));

		// Delete
		repository.delete(fileData.getPeerChunkMetadata().getFragmentUUID());

		// Trying to select again
		retrievedItem = repository.select(fileData.getPeerChunkMetadata().getFragmentUUID());

		// The retrieved item should be NULL at this point
		Assert.assertNull(retrievedItem);
	}

	@Test
	public void testInsertAndUpdate() throws NoSuchAlgorithmException, NoSuchProviderException, RepositoryException {

		Random rand = new Random();
		byte[] data = new byte[2048];
		rand.nextBytes(data);

		// Create object to insert
		PeerChunkData fileData = new PeerChunkData(new PeerChunkMetadata(UUID.randomUUID().toString()
				+ rand.nextInt(), 2048, "30 - 1912", "-1882", Types.Private),
				new Chunk(data, rand.nextInt()));

		// Insert
		repository.insert(fileData);

		// Modify the object
		String modifiedChallenge = "30 + 2012";
		fileData.getPeerChunkMetadata().setChallengeAssociated(modifiedChallenge);
		repository.update(fileData);

		// Retrieve
		PeerChunkData retrievedItem = repository.select(fileData.getPeerChunkMetadata().getFragmentUUID());

		Assert.assertTrue(modifiedChallenge.equals(retrievedItem.getPeerChunkMetadata().getChallengeAssociated()));
	}
}
