package com.paywithbytes.communication.test;

import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Test;

import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.communication.beans.DeleteRequest;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.RetrieveRequest;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.communication.http.helpers.HttpServerHelperSingleton;

public class DeleteRetrieveTest {

	@Test
	public void testDeleteRetrieveGet() throws CommunicationException {

		final String fragmentUUID = "1912";

		// Start the server
		Thread httpServerThread = new Thread(HttpServerHelperSingleton.getInstance());
		httpServerThread.start();

		// Initialized client helper
		HttpClientHelper hch = new HttpClientHelper();

		// Do Delete
		String jsonValueDelete = JsonHelper.toJson(new DeleteRequest(fragmentUUID), DeleteRequest.class);
		RequestResponse response = hch.doDelete(jsonValueDelete, TestLocalHost.getHostInfo(), AppConfig.httpURLDelete);
		Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_OK);

		// Do Retrieve
		String jsonValueRetrieve = JsonHelper.toJson(new RetrieveRequest(fragmentUUID, 10.0), RetrieveRequest.class);
		response = hch.doGet(jsonValueRetrieve, TestLocalHost.getHostInfo(), AppConfig.httpURLRetrieve);

		Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_NOT_FOUND);

		// Kill server thread
		httpServerThread.interrupt();

	}
}
