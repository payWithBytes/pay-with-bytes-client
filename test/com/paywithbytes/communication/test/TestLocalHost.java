package com.paywithbytes.communication.test;

import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.communication.host.Host;

public class TestLocalHost {

	public static Host getHostInfo() {
		return new Host("0.0.0.0", Integer.parseInt(AppConfig.httpServerPort), "http");
	}
}
