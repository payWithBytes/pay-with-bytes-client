package com.paywithbytes.communication.test;

import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.beans.DeleteRequest;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.RetrieveRequest;
import com.paywithbytes.communication.beans.StoreRequest;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.communication.http.helpers.HttpServerHelperSingleton;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.PeerChunkData;
import com.paywithbytes.persistency.beans.PeerChunkMetadata;
import com.paywithbytes.persistency.beans.Types;

public class RetrieveStoreDeleteRequestTest {

	private Logger log = LogHelper.getLogger(RetrieveStoreDeleteRequestTest.class);

	private PeerChunkData randomDataToStore(String fragmentUUID) {
		byte[] data = new byte[2048];
		Random rand = new Random();
		rand.nextBytes(data);
		Chunk chunk = new Chunk(data, rand.nextInt());

		return new PeerChunkData(new PeerChunkMetadata(fragmentUUID, 4000, "2 + 498", Double.valueOf("500").toString(),
				Types.Public), chunk);
	}

	@Test
	public void testDeleteStoreRetrieve() {
		// Start the server
		Thread httpServerThread = new Thread(HttpServerHelperSingleton.getInstance());
		httpServerThread.start();

		try {
			final String fragmentUUID = "1912";

			// Initialized client helper
			HttpClientHelper hch = new HttpClientHelper();

			// Do Delete
			String jsonValueDelete = JsonHelper.toJson(new DeleteRequest(fragmentUUID), DeleteRequest.class);
			RequestResponse response = hch.doDelete(jsonValueDelete, TestLocalHost.getHostInfo(),
					AppConfig.httpURLDelete);
			Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_OK);

			// Do Store
			PeerChunkData localPeerData = this.randomDataToStore(fragmentUUID);
			String jsonValue = JsonHelper.toJson(new StoreRequest("4500", localPeerData), StoreRequest.class);
			response = hch.doPost(jsonValue, TestLocalHost.getHostInfo(), AppConfig.httpURLStore);
			Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_CREATED);

			// Do Retrieve
			String jsonValueRetrieve = JsonHelper
					.toJson(new RetrieveRequest(fragmentUUID, Double.valueOf(localPeerData.getPeerChunkMetadata()
							.getChallengeSolution())), RetrieveRequest.class);

			response = hch.doGet(jsonValueRetrieve, TestLocalHost.getHostInfo(), AppConfig.httpURLRetrieve);
			PeerChunkData recoveredPeerData = JsonHelper.fromJson(response.getValue(), PeerChunkData.class);

			// Compare local and retrieved data
			Assert.assertTrue(localPeerData.equals(recoveredPeerData));

		} catch (Exception e) {
			log.error("Test failed", e);
			Assert.assertTrue(false);
		} finally {
			// Kill server thread
			httpServerThread.interrupt();
		}
	}

	@Test
	public void testDeleteStoreRetrieveGet() {

		final String fragmentUUID = "1912";

		// Start the server
		Thread httpServerThread = new Thread(HttpServerHelperSingleton.getInstance());
		httpServerThread.start();

		try {

			// Initialized client helper
			HttpClientHelper hch = new HttpClientHelper();

			// Do Delete
			String jsonValueDelete = JsonHelper.toJson(new DeleteRequest(fragmentUUID), DeleteRequest.class);
			RequestResponse response = hch.doDelete(jsonValueDelete, TestLocalHost.getHostInfo(),
					AppConfig.httpURLDelete);
			Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_OK);

			// Do Store
			PeerChunkData localPeerData = this.randomDataToStore(fragmentUUID);
			String jsonValue = JsonHelper.toJson(new StoreRequest("4500", localPeerData), StoreRequest.class);
			response = hch.doPost(jsonValue, TestLocalHost.getHostInfo(), AppConfig.httpURLStore);
			Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_CREATED);

			// Do Retrieve
			String jsonValueRetrieve = JsonHelper.toJson(
					new RetrieveRequest(fragmentUUID, Double.valueOf(localPeerData.getPeerChunkMetadata()
							.getChallengeSolution())), RetrieveRequest.class);
			response = hch.doGet(jsonValueRetrieve, TestLocalHost.getHostInfo(), AppConfig.httpURLRetrieve);
			PeerChunkData recoveredPeerData = JsonHelper.fromJson(response.getValue(), PeerChunkData.class);

			// Compare local and retrieved data
			Assert.assertTrue(localPeerData.equals(recoveredPeerData));
		} catch (Exception e) {
			log.error("Test failed", e);
			Assert.assertTrue(false);
		} finally {
			// Kill server thread
			httpServerThread.interrupt();
		}

	}

	@Test
	public void testRetrieveWrongChallengeSolution() throws Exception {

		final String fragmentUUID = "1912";

		// Start the server
		Thread httpServerThread = new Thread(HttpServerHelperSingleton.getInstance());
		httpServerThread.start();

		try {
			// Initialized client helper
			HttpClientHelper hch = new HttpClientHelper();

			// Do Delete
			String jsonValueDelete = JsonHelper.toJson(new DeleteRequest(fragmentUUID), DeleteRequest.class);
			RequestResponse response = hch.doDelete(jsonValueDelete, TestLocalHost.getHostInfo(),
					AppConfig.httpURLDelete);
			Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_OK);

			// Do Store
			PeerChunkData localPeerData = this.randomDataToStore(fragmentUUID);
			String jsonValue = JsonHelper.toJson(new StoreRequest("4500", localPeerData), StoreRequest.class);
			response = hch.doPost(jsonValue, TestLocalHost.getHostInfo(), AppConfig.httpURLStore);
			Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_CREATED);

			// Do Retrieve with modified challenge solution (should yield an error)
			String jsonValueRetrieve = JsonHelper.toJson(
					new RetrieveRequest(fragmentUUID, Double.valueOf(localPeerData.getPeerChunkMetadata()
							.getChallengeSolution()) + 100), RetrieveRequest.class);
			response = hch.doGet(jsonValueRetrieve, TestLocalHost.getHostInfo(), AppConfig.httpURLRetrieve);

			// The Peer should deny our request.
			Assert.assertTrue(response.getStatus() == HttpServletResponse.SC_FORBIDDEN);

		} catch (Exception e) {
			log.error("Test failed", e);
			Assert.assertTrue(false);
		} finally {
			// Kill server thread
			httpServerThread.interrupt();
		}

	}

}
