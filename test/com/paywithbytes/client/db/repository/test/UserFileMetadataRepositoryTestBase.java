package com.paywithbytes.client.db.repository.test;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.client.db.repository.UserFileMetadataRepository;
import com.paywithbytes.common.encryption.SymmetricEncryptionHelper;
import com.paywithbytes.jfec.wrapper.EncodingMetadata;
import com.paywithbytes.persistency.beans.Challenge;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

/**
 * Test the Repository interface. This is just the Base, not the actual test.
 * 
 * @param <T>
 */
public abstract class UserFileMetadataRepositoryTestBase<T extends UserFileMetadataRepository> {

	private T repository;

	protected abstract T createRepositoryInstance();

	@Before
	public void setUp() {
		repository = createRepositoryInstance();
	}

	@Test
	public void testInsertAndSelect() throws NoSuchAlgorithmException, NoSuchProviderException, RepositoryException {
		SymmetricEncryptionHelper encHelper = new SymmetricEncryptionHelper();

		// Create object to insert
		Challenge challenge = new Challenge("1912", "+", "30");
		UserFileMetadata fileMetadata = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				encHelper.getRandomKey(), encHelper.getRandomIV(),
				challenge, 1942.0, Types.Public, 123123, new EncodingMetadata(5, 3, 123123));

		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));

		// Insert
		repository.insert(fileMetadata);

		// Retrieve
		UserFileMetadata retrievedItem = repository.select(fileMetadata.getUUID());

		// Compare
		Assert.assertTrue(fileMetadata.equals(retrievedItem));
	}

	@Test
	public void testInsertAndDeleteAndSelect() throws NoSuchAlgorithmException, NoSuchProviderException,
			RepositoryException {
		SymmetricEncryptionHelper encHelper = new SymmetricEncryptionHelper();

		// Create object to insert
		Challenge challenge = new Challenge("1912", "+", "30");
		UserFileMetadata fileMetadata = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				encHelper.getRandomKey(), encHelper.getRandomIV(),
				challenge, 1942.0, Types.Public, 123123, new EncodingMetadata(5, 3, 3000));

		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));

		// Insert
		repository.insert(fileMetadata);

		// Retrieve
		UserFileMetadata retrievedItem = repository.select(fileMetadata.getUUID());

		// Compare
		Assert.assertTrue(fileMetadata.equals(retrievedItem));

		// Delete
		repository.delete(fileMetadata.getUUID());

		// Trying to select again
		retrievedItem = repository.select(fileMetadata.getUUID());

		// The retrieved item should be NULL at this point
		Assert.assertNull(retrievedItem);
	}

	@Test
	public void testInsertAndUpdate() throws NoSuchAlgorithmException, NoSuchProviderException, RepositoryException {
		SymmetricEncryptionHelper encHelper = new SymmetricEncryptionHelper();

		// Create object to insert
		Challenge challenge = new Challenge("1912", "+", "30");
		UserFileMetadata fileMetadata = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				encHelper.getRandomKey(), encHelper.getRandomIV(), challenge, 1942.0, Types.Public, 123123,
				new EncodingMetadata(5, 3, 400));

		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));

		// Insert
		repository.insert(fileMetadata);

		// Modify the object
		Challenge modifiedChallenge = new Challenge("20", "+", "50");
		fileMetadata.setChallengeAssociated(modifiedChallenge);
		repository.update(fileMetadata);

		// Retrieve
		UserFileMetadata retrievedItem = repository.select(fileMetadata.getUUID());

		Assert.assertTrue(modifiedChallenge.equals(retrievedItem.getChallengeAssociated()));
	}

}
