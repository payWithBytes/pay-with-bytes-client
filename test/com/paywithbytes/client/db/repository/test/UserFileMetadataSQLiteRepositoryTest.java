package com.paywithbytes.client.db.repository.test;

import com.paywithbytes.client.db.repository.UserFileMetadataRepository;
import com.paywithbytes.client.db.repository.UserFileMetadataSQLiteRepository;

/**
 * This initializes the actual SQLite Repository for the base test.
 * 
 */
public class UserFileMetadataSQLiteRepositoryTest extends
		UserFileMetadataRepositoryTestBase<UserFileMetadataRepository> {

	@Override
	protected UserFileMetadataRepository createRepositoryInstance() {
		return new UserFileMetadataSQLiteRepository();
	}

}
