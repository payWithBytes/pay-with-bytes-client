package com.paywithbytes.common.encryption.test;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.paywithbytes.common.encryption.PublicKeyEncryptionHelper;
import com.paywithbytes.common.encryption.SymmetricEncryptionHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.jfec.wrapper.EncodingMetadata;
import com.paywithbytes.persistency.beans.Challenge;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

/**
 * Test the Public Private Key encryption class
 * 
 */
public class PublicKeyEncryptionHelperTest {

	@Test
	public void basicTest() throws Exception {
		// Generate random Public-Private keys
		KeyPair kp = PublicKeyEncryptionHelper.generateRandomPublicPrivateKeys();
		PublicKey pubk = kp.getPublic();
		PrivateKey prvk = kp.getPrivate();

		PublicKeyEncryptionHelper encryptionHelper = new PublicKeyEncryptionHelper();
		// Generate data to encrypt
		byte[] dataBytes = "J2EE Security for Servlets, EJBs and Web Services".getBytes();

		// Encrypt data
		byte[] encBytes = encryptionHelper.encrypt(dataBytes, pubk);

		// Decrypt data
		byte[] decBytes = encryptionHelper.decrypt(encBytes, prvk);

		// Decrypted data and original data should be exactly the same
		Assert.assertTrue(Arrays.equals(dataBytes, decBytes));
	}

	@Test
	public void userFileMetadataTest() throws Exception {
		// Generate random Public-Private keys
		KeyPair kp = PublicKeyEncryptionHelper.generateRandomPublicPrivateKeys();
		PublicKey pubk = kp.getPublic();
		PrivateKey prvk = kp.getPrivate();

		// Generate data to encrypt
		UserFileMetadata fileMetadata = this.createUserFileMetadataObject();

		// Encrypt data
		UserFileMetadataWrapper userFileMetadataWrapper = PublicKeyEncryptionHelper.encryptUserFileMetadata(
				fileMetadata, pubk);

		// Decrypt data
		UserFileMetadata recoveredMetadata = PublicKeyEncryptionHelper.decryptUserFileMetadata(userFileMetadataWrapper,
				prvk);

		// Decrypted data and original data should be exactly the same
		Assert.assertTrue(fileMetadata.equals(recoveredMetadata));
	}

	private UserFileMetadata createUserFileMetadataObject() throws NoSuchAlgorithmException, NoSuchProviderException {

		SymmetricEncryptionHelper encryptHelper = new SymmetricEncryptionHelper();

		Challenge challenge = new Challenge("1912", "+", "30");
		UserFileMetadata fileMetadata = new UserFileMetadata(UUID.randomUUID().toString(), "ThankYou.txt",
				encryptHelper.getRandomKey(), encryptHelper.getRandomIV(),
				challenge, 1942.0, Types.Public, 123123, new EncodingMetadata(5, 3, 123123));

		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-1", 1912, "192.168.2.1"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-2", 1912, "192.168.2.2"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-3", 1912, "192.168.2.3"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-4", 1912, "192.168.2.4"));
		fileMetadata.addUserChunkMetadataItem(new UserChunkMetadata("u2", "u2-5", 1912, "192.168.2.5"));

		return fileMetadata;
	}

	@Test
	public void instantiatePublicAndPrivateKey() throws Exception {

		PublicKeyEncryptionHelper encryption = new PublicKeyEncryptionHelper();

		Assert.assertTrue(AppConfig.userPrivateKey != null && AppConfig.userPublicKey != null);

		byte[] test = "Tu sonrisa que nos hace temblar!".getBytes();

		byte[] encryptedTest = encryption.encrypt(test, AppConfig.userPublicKey);

		byte[] unencryptedTest = encryption.decrypt(encryptedTest, AppConfig.userPrivateKey);

		Assert.assertTrue(Arrays.equals(test, unencryptedTest));
	}

}
