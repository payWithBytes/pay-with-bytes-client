package com.paywithbytes.common.encryption.test;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Assert;
import org.junit.Test;

import com.paywithbytes.common.encryption.SymmetricEncryptionHelper;

/**
 * Test the Symmetric AES encryption.
 * 
 */
public class SymmetricEncryptionTest {

	@Test
	public void testAES() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException {

		SymmetricEncryptionHelper encryptionHelper = new SymmetricEncryptionHelper();

		// Generate random symmetric key.
		byte[] key = encryptionHelper.getRandomKey();

		// Generate data for the test
		String message = "I may not make it through the night, I won't go home without you.";
		// Generate the initialization vector
		byte[] iv = encryptionHelper.getRandomIV();
		// Encrypt data
		byte[] encrypted = encryptionHelper.encrypt(message.getBytes(), key, iv);
		// Decrypt data
		byte[] decrypted = encryptionHelper.decrypt(encrypted, key, iv);
		// The decrypted data and the original data should be the same.
		Assert.assertTrue(Arrays.equals(message.getBytes(), decrypted));
	}

}
