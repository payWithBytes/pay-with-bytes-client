package com.paywithbytes.common.challenges.test;

import javax.script.ScriptException;

import org.junit.Assert;
import org.junit.Test;

import com.paywithbytes.common.utils.ChallengeUtil;
import com.paywithbytes.persistency.beans.Challenge;

/**
 * Test the challenge solver
 * 
 */
public class ChallengesTest {

	@Test
	public void runRandomTest() throws ScriptException {

		Challenge c1 = ChallengeUtil.generateRandomChallenge();
		double solutionOne = ChallengeUtil.solveChallenge(c1);
		double solutionTwo = ChallengeUtil.solveChallenge(c1);

		Assert.assertTrue(solutionOne == solutionTwo);

	}
}
