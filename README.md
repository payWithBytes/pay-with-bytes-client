This is the PayWithBytes client software.





THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Installation Requirements
1. Instal "Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files 7”. http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html

	Mac Os installations instructions:

		-Download the .zip files, uncompress it, and get the two jars.
		-Put this jars in this directory:
			/Library/Java/JavaVirtualMachines/jdk1.7.0_25.jdk/Contents/Home/jre/lib/security
		-This will allow to encrypt with AES-256, which is not supported out ofthe box in Java 7.


2.



Commands
There are 6 commands available to make the system work.

1. Install example
java -jar pwb-client.jar Install

2. Store example
java -jar pwb-client.jar StoreCommand -f "hola.txt" -n 6 -k 3 -t Public

3. Retrieve example
java -jar pwb-client.jar RetrieveCommand UUID

4. Delete example
java -jar pwb-client.jar DeleteCommand UUID

5. Recover Metadata from Server example
java -jar pwb-client.jar Recover

6. Run HTTP Server example
java -jar pwb-client.jar RunServer






