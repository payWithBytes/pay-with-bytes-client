package com.paywithbytes.commands;

import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.communication.commands.helpers.recover.RecoverRemotely;
import com.paywithbytes.communication.commands.helpers.recover.RecoverRemotelySimple;

public class RecoveryCommand extends Command {

	@Override
	public void execute() throws Exception {

		RecoverRemotely recover = new RecoverRemotelySimple();
		recover.recover(AppConfig.userUUID);

	}

	@Override
	public void initFromArguments(String[] args) {

	}

	/**
	 * Testing purposes only
	 * 
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		Command command = new RecoveryCommand();

		command.execute();
	}

}
