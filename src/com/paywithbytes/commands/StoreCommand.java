package com.paywithbytes.commands;

import java.io.File;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.List;
import java.util.UUID;

import javax.script.ScriptException;

import com.google.gson.reflect.TypeToken;
import com.paywithbytes.client.db.repository.UserFileMetadataRepository;
import com.paywithbytes.client.db.repository.UserFileMetadataSQLiteRepository;
import com.paywithbytes.common.encryption.SymmetricEncryptionHelper;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.common.utils.ChallengeUtil;
import com.paywithbytes.communication.commands.helpers.store.StoreRemotely;
import com.paywithbytes.communication.commands.helpers.store.StoreRemotelyMultipleThreads;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.jfec.file.wrapper.FileEncodeWrapper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.jfec.wrapper.EncodingMetadata;
import com.paywithbytes.persistency.beans.Challenge;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class StoreCommand extends Command {

	private String filePath;
	private int n;
	private int k;
	private Types t;
	List<Host> hostList;

	/**
	 * 
	 * @param accesibilityType
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws ScriptException
	 */
	private UserFileMetadata createUserFileMetadata(Types documentType) throws NoSuchAlgorithmException,
			NoSuchProviderException, ScriptException {
		String uuid = UUID.randomUUID().toString();
		File f = new File(this.filePath);
		long fileLength = f.length();
		String fileName = Paths.get(this.filePath).getFileName().toString();
		EncodingMetadata encodingMetadata = new EncodingMetadata(this.n, this.k, fileLength);
		SymmetricEncryptionHelper encryptionHelper = new SymmetricEncryptionHelper();
		Challenge challenge = ChallengeUtil.generateRandomChallenge();
		Double challengeSolution = ChallengeUtil.solveChallenge(challenge);
		// 10 days
		long defaultTimestamp = 10 * 60 * 60 * 24;

		return new UserFileMetadata(uuid, fileName, encryptionHelper.getRandomKey(), encryptionHelper.getRandomIV(),
				challenge, challengeSolution, documentType, defaultTimestamp, encodingMetadata);
	}

	@Override
	public void execute() throws Exception {
		long currentMillis = System.currentTimeMillis();

		// Create the N chunks with redundancy
		FileEncodeWrapper few = new FileEncodeWrapper(filePath, n, k);
		Chunk[] fullChunks = few.encodeFromFile();

		// Store the data remotely
		UserFileMetadata userFileMetadata = this.createUserFileMetadata(this.t);
		StoreRemotely storeRemotely = new StoreRemotelyMultipleThreads();
		storeRemotely.storeRemotely(userFileMetadata, fullChunks, hostList);

		// Store the metadata locally
		UserFileMetadataRepository repo = new UserFileMetadataSQLiteRepository();
		repo.insert(userFileMetadata);

		log.info("(multi thread program) Time elapsed [" + (System.currentTimeMillis() - currentMillis) + "]");

	}

	@Override
	public void initFromArguments(String[] args) {
		int initCount = 0;

		// TODO: Fix this, there are cleaner ways to parse the parameters.
		for (int i = 1; i < args.length; i++) {
			String val = args[i];

			switch (val) {
				case "-n":
					i++;
					this.n = Integer.parseInt(args[i]);
					initCount++;
					break;
				case "-k":
					i++;
					this.k = Integer.parseInt(args[i]);
					initCount++;
					break;
				case "-f":
					i++;
					this.filePath = args[i];
					initCount++;
					break;
				case "-t":
					i++;
					this.t = Types.valueOf(args[i]);
					initCount++;
					break;
				default:
					log.info(String.format("Option [%s] not found. Going to cancel Store Command...", val));
			}
		}

		if (initCount == 4) {
			this.hostList = JsonHelper.fromJsonArray(AppConfig.peerList, new TypeToken<List<Host>>() {
			});
			// We only need n of them.
			this.hostList = this.hostList.subList(0, n);

		}
	}

	/**
	 * Only for test purposes.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		StoreCommand sc = new StoreCommand();

		sc.filePath = "/Users/jsanta/Dropbox/Documents/devel/workspace-kepler/payWithBytes-client/10MB_output.dat";
		sc.n = 3;
		sc.k = 2;
		sc.t = Types.Public;

		sc.hostList = JsonHelper.fromJsonArray(AppConfig.peerList, new TypeToken<List<Host>>() {
		});

		// We only need n of them.
		sc.hostList = sc.hostList.subList(0, sc.n);

		sc.execute();

	}

}
