package com.paywithbytes.commands;

import org.apache.log4j.Logger;

import com.paywithbytes.common.utils.LogHelper;

abstract class Command {

	protected static final Logger log = LogHelper.getLogger(Command.class);

	public abstract void execute() throws Exception;

	public abstract void initFromArguments(String args[]);

}
