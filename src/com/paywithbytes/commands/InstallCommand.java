package com.paywithbytes.commands;

import com.paywithbytes.client.db.repository.UserTableCreation;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.peer.db.repository.PeerTableCreation;

public class InstallCommand extends Command {

	@Override
	public void execute() throws Exception {
		// Create User table
		UserTableCreation utc = new UserTableCreation();
		utc.createTableWithIndex();

		// Create Peer table
		PeerTableCreation ptc = new PeerTableCreation();
		ptc.createTableWithIndex();

		// Generate Public and Private Key Pair
		AppConfig.addPublicAndPrivateKey();

		// Generate User UUID
		AppConfig.addUserUUID();

		// Load Keys
		AppConfig.loadPrivateKey();
		AppConfig.loadPublicKey();
	}

	@Override
	public void initFromArguments(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * Just to run it manually when developing.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		InstallCommand command = new InstallCommand();
		command.execute();
	}

}
