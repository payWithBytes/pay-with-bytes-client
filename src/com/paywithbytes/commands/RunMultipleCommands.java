package com.paywithbytes.commands;

import org.apache.log4j.Logger;

import com.paywithbytes.common.utils.LogHelper;

public class RunMultipleCommands {

	private static final Logger log = LogHelper.getLogger(RunMultipleCommands.class);

	private static final String STORE_COMMAND_STR = "StoreCommand";
	private static final String RETRIEVE_COMMAND_STR = "RetrieveCommand";
	private static final String DELETE_COMMAND_STR = "DeleteCommand";
	private static final String RUN_SERVER_COMMAND_STR = "RunServer";
	private static final String INSTALL_COMMAND_STR = "Install";
	private static final String RECOVER_COMMAND_STR = "Recover";

	public static void main(String args[]) {

		Command command = null;

		System.out.println("args[0] == " + args[0]);

		if (STORE_COMMAND_STR.equalsIgnoreCase(args[0])) {
			log.info("Going to execute Store command");
			command = new StoreCommand();
		} else if (RETRIEVE_COMMAND_STR.equalsIgnoreCase(args[0])) {
			log.info("Going to execute Retrieve command");
			command = new RetrieveCommand();
		} else if (DELETE_COMMAND_STR.equalsIgnoreCase(args[0])) {
			log.info("Run delete command");
			command = new DeleteCommand();
		} else if (RUN_SERVER_COMMAND_STR.equalsIgnoreCase(args[0])) {
			log.info("Going to execute Run server command");
			command = new RunServerCommand();
		} else if (INSTALL_COMMAND_STR.equalsIgnoreCase(args[0])) {
			log.info("Going to execute Install command");
			command = new InstallCommand();
		} else if (RECOVER_COMMAND_STR.equalsIgnoreCase(args[0])) {
			log.info("Going to execute Recover command");
			command = new RecoveryCommand();
		}

		if (command != null) {
			command.initFromArguments(args);
			try {
				command.execute();
			} catch (Exception e) {
				log.error("Error when executing the command", e);
			}
			return;
		}

		log.error("Nothing to execute. Wrong parameters sent.");
		System.exit(-1);

	}
}
