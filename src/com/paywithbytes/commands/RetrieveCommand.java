package com.paywithbytes.commands;

import com.paywithbytes.communication.commands.helpers.retrieve.RetrieveRemotely;
import com.paywithbytes.communication.commands.helpers.retrieve.RetrieveRemotelyMultipleThreads;

public class RetrieveCommand extends Command {

	private String uuid;

	@Override
	public void execute() throws Exception {
		long currentMillis = System.currentTimeMillis();

		RetrieveRemotely retrieve = new RetrieveRemotelyMultipleThreads();
		retrieve.retrieve(uuid);

		log.info("(multi thread program) Time elapsed [" + (System.currentTimeMillis() - currentMillis) + "]");
	}

	@Override
	public void initFromArguments(String[] args) {
		this.uuid = args[1];

	}

	public static void main(String args[]) throws Exception {
		String uuid = "733b5dca-5fb1-4e33-846a-e4db3c2ef6b2";

		RetrieveCommand retrieveCommand = new RetrieveCommand();
		retrieveCommand.uuid = uuid;

		retrieveCommand.execute();
	}

}
