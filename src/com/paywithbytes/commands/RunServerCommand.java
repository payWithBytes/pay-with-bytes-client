package com.paywithbytes.commands;

import com.paywithbytes.communication.http.helpers.HttpServerHelperSingleton;

public class RunServerCommand extends Command {

	@Override
	public void execute() {
		// Start the server
		Thread httpServerThread = new Thread(HttpServerHelperSingleton.getInstance());
		httpServerThread.start();
	}

	@Override
	public void initFromArguments(String[] args) {

	}

}
