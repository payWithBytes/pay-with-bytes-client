package com.paywithbytes.commands;

import com.paywithbytes.client.db.repository.UserFileMetadataRepository;
import com.paywithbytes.client.db.repository.UserFileMetadataSQLiteRepository;
import com.paywithbytes.communication.commands.helpers.delete.DeleteRemotely;
import com.paywithbytes.communication.commands.helpers.delete.DeleteRemotelySimple;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class DeleteCommand extends Command {

	private String uuid;

	@Override
	public void execute() throws Exception {
		UserFileMetadataRepository repo = new UserFileMetadataSQLiteRepository();
		UserFileMetadata metadata = repo.select(uuid);

		DeleteRemotely deleteRemotely = new DeleteRemotelySimple();
		deleteRemotely.delete(metadata);

		repo.delete(metadata.getUUID());
	}

	@Override
	public void initFromArguments(String[] args) {
		this.uuid = args[1];
	}

}
