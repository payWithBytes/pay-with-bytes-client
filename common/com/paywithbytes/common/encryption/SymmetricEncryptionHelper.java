package com.paywithbytes.common.encryption;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.crypto.codec.Base64;

import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserFileMetadata;

/**
 * This class contains the helper about the Symmetric encryption keys.
 * 
 */
public class SymmetricEncryptionHelper {

	private static final String ALGORITHM = "AES";
	private static final String X_TRANSFORMATION = "AES/CBC/PKCS5Padding";

	/**
	 * 
	 * Returns a random generated AES-256 bytes key.
	 * 
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 */
	public byte[] getRandomKey() throws NoSuchAlgorithmException, NoSuchProviderException {

		KeyGenerator keyGen = KeyGenerator.getInstance(ALGORITHM);
		keyGen.init(256);
		SecretKey secretKey = keyGen.generateKey();

		return secretKey.getEncoded();

	}

	/**
	 * Decrypts data with the given key and initialization vector.
	 * 
	 * @param encryptedData
	 * @param key
	 * @param iv
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public byte[] decrypt(byte[] encryptedData, byte[] key, byte[] iv) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
			BadPaddingException {
		IvParameterSpec ivspec = new IvParameterSpec(iv);
		SecretKeySpec skeySpec = new SecretKeySpec(key, ALGORITHM);

		// Initialize the cipher
		Cipher cipher = Cipher.getInstance(X_TRANSFORMATION);

		// Reinitialize the cipher for decryption
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);

		// Decrypt the message
		return cipher.doFinal(encryptedData);
	}

	/**
	 * Encrypt the data received with the given key and initialization vector.
	 * 
	 * @param dataToEncrypt
	 * @param key
	 * @param iv
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public byte[] encrypt(byte[] dataToEncrypt, byte[] key, byte[] iv) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
			BadPaddingException {

		IvParameterSpec ivspec = new IvParameterSpec(iv);
		SecretKeySpec skeySpec = new SecretKeySpec(key, ALGORITHM);

		// Initialize the cipher for encrypt mode
		Cipher cipher = Cipher.getInstance(X_TRANSFORMATION);
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);

		// Encrypt the message
		byte[] encrypted = cipher.doFinal(dataToEncrypt);

		return encrypted;
	}

	/**
	 * Build the initialization vector. This example is all zeros, but it
	 * could be any value or generated using a random number generator.
	 */
	public byte[] getRandomIV() {
		SecureRandom random = new SecureRandom();
		byte iv[] = new byte[16];// generate random 16 byte IV AES is always 16bytes
		random.nextBytes(iv);
		return iv;
	}

	/**
	 * Turns array of bytes into string
	 * 
	 * @param buf
	 *            Array of bytes to convert to hex string
	 * @return Generated hex string
	 */
	public static String asHex(byte buf[]) {
		StringBuilder strbuf = new StringBuilder(buf.length * 2);
		int i;
		for (i = 0; i < buf.length; i++) {
			if (((int) buf[i] & 0xff) < 0x10) {
				strbuf.append("0");
			}
			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}
		return strbuf.toString();
	}

	public static void encryptChunks(Chunk[] chunkArray, UserFileMetadata userFileMetadata)
			throws Exception
	{
		SymmetricEncryptionHelper encryption = new SymmetricEncryptionHelper();

		for (Chunk chunk : chunkArray) {
			byte[] encryptedBytes = encryption.encrypt(chunk.getData(), userFileMetadata.getSymmetricKey(),
					userFileMetadata.getSymmetricKeyIV());
			byte[] encodedBytes = Base64.encode(encryptedBytes);
			chunk.setData(encodedBytes);
		}
	}

	public static void encryptChunk(Chunk chunk, UserFileMetadata userFileMetadata)
			throws Exception
	{
		SymmetricEncryptionHelper encryption = new SymmetricEncryptionHelper();

		byte[] encryptedBytes = encryption.encrypt(chunk.getData(), userFileMetadata.getSymmetricKey(),
				userFileMetadata.getSymmetricKeyIV());
		byte[] encodedBytes = Base64.encode(encryptedBytes);
		chunk.setData(encodedBytes);
	}

	public static void decryptChunks(Chunk[] chunkArray, UserFileMetadata userFileMetadata)
			throws Exception {

		if (userFileMetadata.getType() == Types.Private) {

			SymmetricEncryptionHelper encryption = new SymmetricEncryptionHelper();

			for (Chunk chunk : chunkArray) {
				byte[] decodedBytes = Base64.decode(chunk.getData());
				byte[] decryptedBytes = encryption.decrypt(decodedBytes, userFileMetadata.getSymmetricKey(),
						userFileMetadata.getSymmetricKeyIV());

				chunk.setData(decryptedBytes);
			}
		}
	}

}
