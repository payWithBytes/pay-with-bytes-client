package com.paywithbytes.common.encryption;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.springframework.security.crypto.codec.Base64;

import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.persistency.beans.UserFileMetadata;

/**
 * This class contains the helper about the Public-Private encryption keys.
 * 
 */
public class PublicKeyEncryptionHelper {

	public static final String ALGORITHM = "RSA";
	private static final String X_TRANSFORMATION = "RSA/ECB/PKCS1Padding";

	public byte[] encrypt(byte[] inpBytes, PublicKey key) throws Exception {
		Cipher cipher = Cipher.getInstance(X_TRANSFORMATION);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(inpBytes);
	}

	public byte[] decrypt(byte[] inpBytes, PrivateKey key) throws Exception {
		Cipher cipher = Cipher.getInstance(X_TRANSFORMATION);
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(inpBytes);
	}

	public static KeyPair generateRandomPublicPrivateKeys() throws NoSuchAlgorithmException {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(ALGORITHM);
		kpg.initialize(512); // 512 is the keysize.
		KeyPair kp = kpg.generateKeyPair();

		return kp;
	}

	public static UserFileMetadataWrapper encryptUserFileMetadata(UserFileMetadata userFileMetadata, PublicKey pubk)
			throws Exception
	{
		UserFileMetadataWrapper retVal = new UserFileMetadataWrapper();

		PublicKeyEncryptionHelper publicPrivateKeyEncryption = new PublicKeyEncryptionHelper();
		SymmetricEncryptionHelper symmetricKeyEncryption = new SymmetricEncryptionHelper();

		// Get symmetric key, we are going to use this one to encrypt the entire object
		byte[] symmetricKey = userFileMetadata.getSymmetricKey();
		byte[] iv = userFileMetadata.getSymmetricKeyIV();

		// Encrypt the symmetric key with Public Key encryption
		byte[] encryptedSymmetricKey = publicPrivateKeyEncryption.encrypt(symmetricKey, pubk);

		// Save Encoded key and encoded IV
		retVal.setEncryptedSymmetricKey(Base64.encode(encryptedSymmetricKey));
		retVal.setIv(Base64.encode(iv));

		// Convert UserFileMetadata to JSON
		String jsonValue = JsonHelper.toJson(userFileMetadata, UserFileMetadata.class);
		byte[] jsonValueBytes = jsonValue.getBytes();

		// Encrypt UserFileMetadata
		byte[] encryptedBytes = symmetricKeyEncryption.encrypt(jsonValueBytes, symmetricKey, iv);
		// Encode UserFileMetadata
		byte[] encodedBytes = Base64.encode(encryptedBytes);

		// Save UserFileMetadata
		retVal.setUserFileMetadata(encodedBytes);

		// Set the File Id (1. Get MD5 2. Encrypt 3. Encode)
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		byte[] theDigest = md5.digest(userFileMetadata.getUUID().getBytes());
		byte[] theEncryptedDigest = symmetricKeyEncryption.encrypt(theDigest, symmetricKey, iv);
		byte[] theEncryptedDigestEncoded = Base64.encode(theEncryptedDigest);
		retVal.setFileIdDigest(new String(theEncryptedDigestEncoded));

		return retVal;
	}

	private static String convertToHexString(byte[] strValue) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] theDigest = md.digest(strValue);
		return DatatypeConverter.printHexBinary(theDigest);
	}

	public static UserFileMetadata decryptUserFileMetadata(UserFileMetadataWrapper userFileMetadataWrapper,
			PrivateKey privk)
			throws Exception
	{
		PublicKeyEncryptionHelper publicPrivateKeyEncryption = new PublicKeyEncryptionHelper();
		SymmetricEncryptionHelper symmetricKeyEncryption = new SymmetricEncryptionHelper();

		// Decode symmetric key
		byte[] decodedEncryptedSymmetricKey = Base64.decode(userFileMetadataWrapper.getEncryptedSymmetricKey());
		// Get the un-encrypted symmetric key
		byte[] symmetricKey = publicPrivateKeyEncryption.decrypt(decodedEncryptedSymmetricKey, privk);
		// Decode IV
		byte[] iv = Base64.decode(userFileMetadataWrapper.getIv());

		// Decode UserFileMetadata
		byte[] decodedUserFileMetadataBytes = Base64.decode(userFileMetadataWrapper.getUserFileMetadata());
		byte[] unEncryptedUserFileMetadataBytes = symmetricKeyEncryption.decrypt(
				decodedUserFileMetadataBytes, symmetricKey, iv);

		// Convert UserFileMetadata to JSON
		return JsonHelper.fromJson(new String(unEncryptedUserFileMetadataBytes), UserFileMetadata.class);
	}
}
