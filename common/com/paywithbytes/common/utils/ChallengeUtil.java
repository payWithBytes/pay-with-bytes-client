package com.paywithbytes.common.utils;

import java.util.Random;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.paywithbytes.persistency.beans.Challenge;

/**
 * This utility class creates random challenges and solves them.
 * 
 */
public class ChallengeUtil {

	/**
	 * This is a very fast solution to solve any kind of math challenges (in a string format)
	 * 
	 * However, we should be careful because receiving a "while(true){ 1 + 1 ;}" as a challenge in this case
	 * will probably hang the Virtual Machine "forever".
	 * 
	 * #TODO Code injection in our case could possible be something that we want to take care of. We could
	 * re-write this with a very simple implementation also, we do not need anything advancer nor complicated.
	 * 
	 * 
	 * @param challenge
	 * @return solution
	 * @throws ScriptException
	 */
	public static double solveChallenge(String challenge) throws ScriptException {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("python");
		Object result = engine.eval(challenge);

		return (double) result;
	}

	public static double solveChallengeJavascript(Challenge challenge) throws ScriptException {
		return ChallengeUtil.solveChallenge(challenge.toString());
	}

	public static double solveChallenge(Challenge challenge) {

		Integer firstValue = Integer.parseInt(challenge.getFirstTerm());
		Integer secondValue = Integer.parseInt(challenge.getSecondTerm());

		String retVal = null;

		switch (challenge.getOperator()) {
			case "+":
				retVal = (firstValue + secondValue) + "";
				break;
			case "*":
				retVal = (firstValue * secondValue) + "";
				break;
			default:
				break;
		}

		return Double.parseDouble(retVal);
	}

	public static Challenge generateRandomChallenge() {

		Random rand = new Random();
		Challenge c = new Challenge(rand.nextInt() % 10000 + "", ChallengeUtil.getRandomOperator(), rand.nextInt()
				% 10000 + "");

		return c;
	}

	private static String getRandomOperator() {
		Random rand = new Random();
		String operators[] = { "+", "*" };

		return operators[rand.nextInt() % 1];

	}
}
