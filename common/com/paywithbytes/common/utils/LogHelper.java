package com.paywithbytes.common.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogHelper {

	public static Logger getLogger(Class<?> clazz) {
		PropertyConfigurator.configure(AppConfig.log4JPropertiesRelativePath);

		return Logger.getLogger(clazz);
	}
}
