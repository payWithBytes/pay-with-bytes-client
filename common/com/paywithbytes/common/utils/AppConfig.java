package com.paywithbytes.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Properties;
import java.util.UUID;

import com.paywithbytes.common.encryption.PublicKeyEncryptionHelper;

public class AppConfig {

	private static final String PROPERTIES_FILE_PATH = "configuration-files/AppConfig.properties";

	// App Info
	private static final String APP_NAME_PROP = "com.paywithbytes.common.utils.appname";
	private static final String USER_UUID_PROP = "com.paywithbytes.userinfo.useruuid";
	private static final String KEY_PUBLIC_PROP = "com.paywithbytes.userinfo.publickey";
	private static final String KEY_PRIVATE_PROP = "com.paywithbytes.userinfo.privatekey";

	private static final String HTTP_SERVER_PORT_PROP = "com.paywithbytes.communication.http.server.port";
	private static final String HTTP_URL_STORE_PROP = "com.paywithbytes.communication.http.url.store";
	private static final String HTTP_URL_RETRIEVE_PROP = "com.paywithbytes.communication.http.url.retrieve";
	private static final String HTTP_URL_DELETE_PROP = "com.paywithbytes.communication.http.url.delete";

	private static final String LOG4J_RELATIVE_PATH_PROP = "com.paywithbytes.common.utils.log4j.properties.file";

	private static final String PEER_LIST_PROP = "com.paywithbytes.communication.host.peerlist";

	// Server
	private static final String SERVER_HOST_PROP = "com.paywithbytes.communication.host.server";
	private static final String SERVER_URL_BACKUP_PROP = "com.paywithbytes.communication.host.server.url.backup";
	private static final String SERVER_URL_RECOVERY_PROP = "com.paywithbytes.communication.host.server.url.recovery";
	private static final String SERVER_URL_DELETE_PROP = "com.paywithbytes.communication.host.server.url.delete";

	// App Info
	public static String appName;
	public static PublicKey userPublicKey;
	public static PrivateKey userPrivateKey;
	public static String userUUID;

	// Server info
	public static String httpServerPort;
	public static String httpURLStore;
	public static String httpURLRetrieve;
	public static String httpURLDelete;

	public static String log4JPropertiesRelativePath;

	public static String peerList;
	public static String serverHost;
	public static String serverHttpURLBackup;
	public static String serverHttpURLRecovery;
	public static String serverHttpURLDelete;

	/**
	 * Create only if it does NOT exists
	 * 
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	public static void addPublicAndPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		Properties configProperty = new Properties();
		InputStream input = new FileInputStream(PROPERTIES_FILE_PATH);

		// load the properties file
		configProperty.load(input);

		if (configProperty.containsKey(KEY_PUBLIC_PROP) && configProperty.containsKey(KEY_PRIVATE_PROP)) {
			// No need to re-generate the values if they exist already.
			return;
		}
		input.close();

		configProperty = new Properties();

		KeyPair keyPair = PublicKeyEncryptionHelper.generateRandomPublicPrivateKeys();

		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

		configProperty.setProperty(KEY_PUBLIC_PROP, publicKey.getModulus() + "|" + publicKey.getPublicExponent());
		configProperty.setProperty(KEY_PRIVATE_PROP, privateKey.getModulus() + "|" + privateKey.getPrivateExponent());

		File file = new File(PROPERTIES_FILE_PATH);
		FileOutputStream fileOut = new FileOutputStream(file, true);
		configProperty.store(fileOut, "Pair of Keys for the user");

		fileOut.close();

	}

	public enum KeyType {
		Public, Private
	}

	public static void loadPublicKey() throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException {

		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream(PROPERTIES_FILE_PATH);
		prop.load(input);

		String publicKeyStr = prop.getProperty(KEY_PUBLIC_PROP);

		if (publicKeyStr == null)
			return;

		String[] parts = publicKeyStr.split("\\|");

		RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(
				new BigInteger(parts[0]),
				new BigInteger(parts[1]));

		AppConfig.userPublicKey = KeyFactory.getInstance(PublicKeyEncryptionHelper.ALGORITHM).generatePublic(
				publicKeySpec);

		input.close();
	}

	public static void loadPrivateKey() throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException {

		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream(PROPERTIES_FILE_PATH);
		prop.load(input);

		String privateKeyStr = prop.getProperty(KEY_PRIVATE_PROP);

		if (privateKeyStr == null)
			return;

		String[] parts = privateKeyStr.split("\\|");

		RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(
				new BigInteger(parts[0]),
				new BigInteger(parts[1]));

		AppConfig.userPrivateKey = KeyFactory.getInstance(PublicKeyEncryptionHelper.ALGORITHM).generatePrivate(
				privateKeySpec);

		input.close();
	}

	/**
	 * Create only if it does NOT exists
	 * 
	 * @throws IOException
	 */
	public static void addUserUUID() throws IOException {
		Properties configProperty = new Properties();

		AppConfig.userUUID = UUID.randomUUID().toString();

		configProperty.setProperty(USER_UUID_PROP, AppConfig.userUUID);

		File file = new File(PROPERTIES_FILE_PATH);
		FileOutputStream fileOut = new FileOutputStream(file, true);
		if (configProperty.get(USER_UUID_PROP) == null)
			configProperty.store(fileOut, "user ID");

		fileOut.close();
	}

	static {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(PROPERTIES_FILE_PATH);

			// load the properties file
			prop.load(input);

			// get the properties values
			AppConfig.appName = prop.getProperty(APP_NAME_PROP);
			AppConfig.loadPrivateKey();
			AppConfig.loadPublicKey();

			// Server properties
			AppConfig.httpServerPort = prop.getProperty(HTTP_SERVER_PORT_PROP);
			AppConfig.httpURLStore = prop.getProperty(HTTP_URL_STORE_PROP);
			AppConfig.httpURLRetrieve = prop.getProperty(HTTP_URL_RETRIEVE_PROP);
			AppConfig.httpURLDelete = prop.getProperty(HTTP_URL_DELETE_PROP);

			AppConfig.log4JPropertiesRelativePath = prop.getProperty(LOG4J_RELATIVE_PATH_PROP);

			AppConfig.peerList = prop.getProperty(PEER_LIST_PROP);

			// Server Config
			AppConfig.serverHost = prop.getProperty(SERVER_HOST_PROP);
			AppConfig.serverHttpURLBackup = prop.getProperty(SERVER_URL_BACKUP_PROP);
			AppConfig.serverHttpURLRecovery = prop.getProperty(SERVER_URL_RECOVERY_PROP);
			AppConfig.serverHttpURLDelete = prop.getProperty(SERVER_URL_DELETE_PROP);

			// User Config
			AppConfig.userUUID = prop.getProperty(USER_UUID_PROP);

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
