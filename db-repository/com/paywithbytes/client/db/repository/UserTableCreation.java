package com.paywithbytes.client.db.repository;

import java.sql.SQLException;

import com.paywithbytes.common.db.TableCreationHelper;

/**
 * This class contains the information to create the table in the database that will
 * hold the User data and metadata.
 * 
 */
public class UserTableCreation extends TableCreationHelper {

	/**
	 * Table description: We basically want to store all of the information from
	 * the class UserFileMetadata.
	 * 
	 * 1. UUID = 36 characters (including -)
	 * 2. File Name = 255 max length
	 * 3. Symmetric Key = AES-256 has 256 bits keys, which means 256/8 = 32 bytes
	 * 4. ChallengeAssociated = 25 max length
	 * 5. Type = (public, private, anonymous(?))
	 * 6. TotalLength = big int
	 * 7. TimeStamp = big int
	 * 8. ChunksMetadata = 4096 max length (string). This field will contain an array of json values
	 * (See the ChunkMetadata class)
	 * 
	 **/
	public static final String USER_FILES_TABLE_NAME = "User_Files";
	public static final String USER_FILES_TABLE_PK_NAME = "FileUUID";

	public static final String USER_FILES_TABLE_CREATION_QUERY =
			"create table if not exists " + USER_FILES_TABLE_NAME +
					" (" + USER_FILES_TABLE_PK_NAME + " varchar(40) primary key not null," +
					" FileName varchar(255) not null, " +
					" SymmetricKey varchar(40), " +
					" SymmetricKeyIV varchar(40), " +
					" ChallengeAssociated varchar(25), " +
					" Type varchar(2) not null, " +
					// " TotalLength unsigned big int not null," +
					" TimeStamp unsigned bit int not null, " +
					" EncodingMetadata varchar(256) not null, " +
					" ChunksMetadata varchar(8192) not null)";

	public static final String USER_FILES_CREATE_INDEX =
			"create unique index if not exists 'FileUUIDIndex' on " + USER_FILES_TABLE_NAME + "('FileUUID')";

	public void createTableWithIndex() throws SQLException {
		TableCreationHelper dbHelper = new TableCreationHelper();
		dbHelper.executeUpdate(USER_FILES_TABLE_CREATION_QUERY);
		dbHelper.executeUpdate(USER_FILES_CREATE_INDEX);
	}

	public static void main(String[] args) throws SQLException {

		// final String uuid = UUID.randomUUID().toString();
		// System.out.println(uuid);
	}

}
