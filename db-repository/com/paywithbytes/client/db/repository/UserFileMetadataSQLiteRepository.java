package com.paywithbytes.client.db.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.db.ConnectionHelper;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.ChallengeUtil;
import com.paywithbytes.jfec.wrapper.EncodingMetadata;
import com.paywithbytes.persistency.beans.Challenge;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

/**
 * This is the implementation in SQLite for the UserFileMetadataRepository interface.
 * 
 */
public class UserFileMetadataSQLiteRepository extends ConnectionHelper implements UserFileMetadataRepository {

	private static final String tableName = UserTableCreation.USER_FILES_TABLE_NAME;
	private static final String tablePKName = UserTableCreation.USER_FILES_TABLE_PK_NAME;

	@Override
	public void insert(UserFileMetadata fileMetadata) throws RepositoryException {

		PreparedStatement prep = null;
		try {
			String preSqlStmt = String.format("insert into %s values(?,?,?,?,?,?,?,?,?);", tableName);
			prep = getConnection().prepareStatement(preSqlStmt);
			/*
			 * 1. UUID = 36 characters (including -)
			 * 2. File Name = 255 max length
			 * 3. Symmetric Key = AES-256 has 256 bits keys, which means 256/8 = 32 bytes
			 * 4. " SymmetricKeyIV varchar(40)" +
			 * 5. ChallengeAssociated = 25 max length
			 * 6. Type = (public, private, anonymous(?))
			 * 7. TotalLength = big int
			 * 8. TimeStamp = big int
			 * 9. ChunksMetadata = blob. This field will contain an array of json values
			 */
			prep.setString(1, fileMetadata.getUUID());
			prep.setString(2, fileMetadata.getFileName());
			prep.setBytes(3, fileMetadata.getSymmetricKey());
			prep.setBytes(4, fileMetadata.getSymmetricKeyIV());
			prep.setString(5, JsonHelper.toJson(fileMetadata.getChallengeAssociated(), Challenge.class));
			prep.setString(6, fileMetadata.getType().toString());
			prep.setLong(7, fileMetadata.getTimeStamp());
			prep.setString(8, JsonHelper.toJson(fileMetadata.getEncodingMetadata(), EncodingMetadata.class));
			prep.setString(9, JsonHelper.toJson(fileMetadata.getUserChunkMetadataList(), List.class));

			prep.execute();
			log.info(String.format("Records inserted successfully : metadata UUID [%s]", fileMetadata.getUUID()));

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when inserting data UUID [%s]", fileMetadata.getUUID()),
					e);
		} finally {
			if (prep != null)
				try {
					prep.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	@Override
	public void delete(String uuid) throws RepositoryException {
		String preSqlStmt = String.format("delete from %s where %s = ?", tableName, tablePKName);

		PreparedStatement prep = null;

		try {
			prep = getConnection().prepareStatement(preSqlStmt);
			prep.setString(1, uuid);
			prep.executeUpdate();
			log.info(String.format("Record deleted successfully : metadata UUID [%s]", uuid));
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when inserting data UUID [%s]", uuid), e);
		} finally {
			if (prep != null)
				try {
					prep.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	@Override
	public void update(UserFileMetadata userFileMetadata) throws RepositoryException {
		// TODO Create a real update query, this is not efficient.

		UserFileMetadata retrieveItem = this.select(userFileMetadata.getUUID());

		if (retrieveItem != null) {
			this.delete(retrieveItem.getUUID());
			this.insert(userFileMetadata);
		}
	}

	@Override
	public UserFileMetadata select(String uuid) throws RepositoryException {
		UserFileMetadata retVal = new UserFileMetadata();

		String preSqlStmt = String.format("select * from %s where %s = ?", tableName, tablePKName);

		PreparedStatement prep = null;

		try {
			prep = getConnection().prepareStatement(preSqlStmt);
			prep.setString(1, uuid);

			ResultSet rs = prep.executeQuery();

			/* If the result set is empty, then return null */
			if (!rs.isBeforeFirst()) {
				log.info(String.format("Going to return null, we did not find any object with id [%s]", uuid));
				return null;
			}

			/*
			 * " (" + USER_FILES_TABLE_PK_NAME + " varchar(40) primary key not null," +
			 * " FileName varchar(255) not null, " +
			 * " SymmetricKey varchar(40), " +
			 * " SymmetricKeyIV varchar(40)" +
			 * " ChallengeAssociated varchar(25), " +
			 * " Type varchar(2) not null, " +
			 * " TotalLength unsigned big int not null," +
			 * " TimeStamp unsigned bit int not null, "
			 * + " ChunksMetadata varchar(4096) not null)";
			 */

			retVal.setUUID(rs.getString(1));
			retVal.setFileName(rs.getString(2));
			retVal.setSymmetricKey(rs.getBytes(3));
			retVal.setSymmetricKeyIV(rs.getBytes(4));
			retVal.setChallengeAssociated(JsonHelper.fromJson(rs.getString(5), Challenge.class));
			retVal.setType(Types.valueOf(rs.getString(6)));
			retVal.setTimeStamp(rs.getLong(7));
			retVal.setEncodingMetadata(JsonHelper.fromJson(rs.getString(8), EncodingMetadata.class));
			retVal.setUserChunkMetadataList(JsonHelper.fromJsonArray(rs.getString(9),
					new TypeToken<List<UserChunkMetadata>>() {
					}));

			// Set Challenge Solution
			retVal.setChallengeSolution(ChallengeUtil.solveChallenge(retVal.getChallengeAssociated()));

			return retVal;

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when inserting data UUID [%s]", uuid),
					e);
		} finally {
			if (prep != null)
				try {
					prep.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}
}
