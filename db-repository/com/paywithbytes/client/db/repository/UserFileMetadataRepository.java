package com.paywithbytes.client.db.repository;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.persistency.beans.UserFileMetadata;

/**
 * We design the Repository with this interface because we might need to change
 * the SQLite implementation some time in the future. If we do that, this interface
 * will make the change easier. Even the junit tests are written for this interface
 * and not for the specific implementation.
 */
public interface UserFileMetadataRepository {

	/**
	 * Insert the userFileMetadata in the database (or storage media)
	 * 
	 * @param userFileMetadata
	 */
	public void insert(UserFileMetadata userFileMetadata) throws RepositoryException;

	/**
	 * Update the userFileMetadata in the database (or storage media)
	 * 
	 * @param userFileMetadata
	 */
	public void update(UserFileMetadata userFileMetadata) throws RepositoryException;

	/**
	 * Select the uuid from the database (or storage media)
	 * 
	 * @param uuid
	 * @return
	 */
	public UserFileMetadata select(String uuid) throws RepositoryException;

	/**
	 * Delete the uuid from the database (or storage media)
	 * 
	 * @param uuid
	 */
	public void delete(String uuid) throws RepositoryException;

}