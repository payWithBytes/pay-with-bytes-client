package com.paywithbytes.common.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.paywithbytes.common.utils.LogHelper;

/**
 * This class contains the connection to the database that it is only initialized once every time
 * we run the application.
 * 
 */
public class ConnectionHelper {

	protected static final Logger log = LogHelper.getLogger(ConnectionHelper.class);
	private static final String classForName = "org.sqlite.JDBC";
	private static final String sqliteConnectionString = "jdbc:sqlite:database/payWithBytesClient.db";
	private static Connection connection;

	static {

		try {
			Class.forName(classForName);
			setConnection(DriverManager.getConnection(ConnectionHelper.sqliteConnectionString));
		} catch (SQLException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
		}

		log.info("Opened database successfully");
	}

	protected static Connection getConnection() {
		return connection;
	}

	private static void setConnection(Connection connection) {
		ConnectionHelper.connection = connection;
	}

	protected void closeConnection() throws SQLException {
		ConnectionHelper.getConnection().close();
	}

}
