package com.paywithbytes.common.db;

import java.sql.Statement;

/**
 * This is a helper that contains a method that creates the tables. It is used
 * by two classes (UserTableCreation and PeerTableCreation).
 * 
 */
public class TableCreationHelper extends ConnectionHelper {

	public TableCreationHelper() {

	}

	/**
	 * The two create tables queries contains the "if not exists" description,
	 * so the table won't be created if it is already in the database.
	 * 
	 * @param sqlCreateQuery
	 */
	public void executeUpdate(String sqlCreateQuery) {

		try (Statement stmt = getConnection().createStatement()) {

			stmt.executeUpdate(sqlCreateQuery);
			stmt.close();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

}
