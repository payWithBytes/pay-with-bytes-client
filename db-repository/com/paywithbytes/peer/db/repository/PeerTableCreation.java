package com.paywithbytes.peer.db.repository;

import java.sql.SQLException;
import java.util.UUID;

import com.paywithbytes.common.db.TableCreationHelper;

/**
 * This class contains the information to create the table in the database that will
 * hold the Peer data and metadata.
 * 
 */
public class PeerTableCreation {

	/**
	 * Table description: We basically want to store all of the information from
	 * the class UserFileMetadata.
	 * 
	 * 1. FragmentUUID = 36 characters (including -) + 9 character indicating the fragment == (45)
	 * 2. Chunk Data json
	 * 3. PeerChunkMetadata = 1024 max length (string). This field will contain an array of json values
	 * (See the PeerChunkMetadata class)
	 * 
	 **/
	public static final String PEER_FILES_TABLE_NAME = "Peer_Files";
	public static final String PEER_FILES_TABLE_PK_NAME = "FragmentUUID";

	private static final String PEER_FILES_TABLE_CREATION_QUERY =
			"create table if not exists " + PEER_FILES_TABLE_NAME +
					" (" + PEER_FILES_TABLE_PK_NAME + " varchar(45) primary key not null," +
					" Chunk varchar(8096) not null, "
					+ " PeerChunkMetadata varchar(1024) not null)";

	private static final String PEER_FILES_CREATE_INDEX =
			"create unique index if not exists 'FragmentUUIDIndex' on " + PEER_FILES_TABLE_NAME + "('FragmentUUID')";

	public void createTableWithIndex() throws SQLException {
		TableCreationHelper dbHelper = new TableCreationHelper();
		dbHelper.executeUpdate(PEER_FILES_TABLE_CREATION_QUERY);
		dbHelper.executeUpdate(PEER_FILES_CREATE_INDEX);
	}

	public static void main(String[] args) throws SQLException {

		final String uuid = UUID.randomUUID().toString();
		System.out.println(uuid);
	}

}
