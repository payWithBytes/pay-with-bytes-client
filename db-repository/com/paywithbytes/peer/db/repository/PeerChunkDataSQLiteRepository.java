package com.paywithbytes.peer.db.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.db.ConnectionHelper;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.PeerChunkData;
import com.paywithbytes.persistency.beans.PeerChunkMetadata;

/**
 * This is the implementation in SQLite for the PeerFileMetadataRepository interface.
 * 
 */
public class PeerChunkDataSQLiteRepository extends ConnectionHelper implements PeerChunkDataRepository {

	private static final String tableName = PeerTableCreation.PEER_FILES_TABLE_NAME;
	private static final String tablePKName = PeerTableCreation.PEER_FILES_TABLE_PK_NAME;

	@Override
	public void insert(PeerChunkData fileData) throws RepositoryException {

		PreparedStatement prep = null;
		try {
			String preSqlStmt = String.format("insert into %s values(?,?,?);", tableName);
			prep = getConnection().prepareStatement(preSqlStmt);
			/*
			 * 1. Fragment UUID
			 * 2. Data blob
			 * 3. PeerChunkMetadata = 1024 max length (string). This field will contain an array of json values
			 * (See the PeerChunkMetadata class)
			 * TODO: we don't need to duplicate the Fragment UUID information, in the PK and in the PeerChunkMetadata
			 */
			prep.setString(1, fileData.getPeerChunkMetadata().getFragmentUUID());
			prep.setString(2, JsonHelper.toJson(fileData.getChunk(), Chunk.class));
			prep.setString(3, JsonHelper.toJson(fileData.getPeerChunkMetadata(), PeerChunkMetadata.class));

			prep.execute();
			log.info(String.format("Records inserted successfully : metadata UUID [%s]", fileData
					.getPeerChunkMetadata().getFragmentUUID()));
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when inserting data UUID [%s]", fileData
					.getPeerChunkMetadata().getFragmentUUID()), e);
		} finally {
			if (prep != null)
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	@Override
	public void delete(String fragmentUuid) throws RepositoryException {
		String preSqlStmt = String.format("delete from %s where %s = ?", tableName, tablePKName);

		PreparedStatement prep = null;

		try {
			prep = getConnection().prepareStatement(preSqlStmt);
			prep.setString(1, fragmentUuid);
			prep.executeUpdate();
			log.info(String.format("Record deleted successfully : metadata UUID [%s]", fragmentUuid));
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when deleting data UUID [%s]", fragmentUuid), e);
		} finally {
			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void update(PeerChunkData peerFileData) throws RepositoryException {
		// TODO Create a real update query, this is not efficient.

		PeerChunkData retrieveItem = this.select(peerFileData.getPeerChunkMetadata().getFragmentUUID());

		if (retrieveItem != null) {
			this.delete(retrieveItem.getPeerChunkMetadata().getFragmentUUID());
			this.insert(peerFileData);
		}
	}

	@Override
	public PeerChunkData select(String fragmentUuid) throws RepositoryException {
		PeerChunkData retVal = new PeerChunkData();

		String preSqlStmt = String.format("select * from %s where %s = ?", tableName, tablePKName);
		PreparedStatement prep = null;

		try {
			prep = getConnection().prepareStatement(preSqlStmt);
			prep.setString(1, fragmentUuid);

			ResultSet rs = prep.executeQuery();

			/* If the result set is empty, then return null */
			if (!rs.isBeforeFirst()) {
				log.info(String.format("Going to return null, we did not find any object with id [%s]", fragmentUuid));
				return null;
			}

			retVal.setChunk(JsonHelper.fromJson(rs.getString(2), Chunk.class));
			retVal.setPeerChunkMetadata(JsonHelper.fromJson(rs.getString(3), PeerChunkMetadata.class));

			return retVal;

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			throw new RepositoryException(String.format("Error when inserting data UUID [%s]", fragmentUuid), e);
		} finally {
			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
