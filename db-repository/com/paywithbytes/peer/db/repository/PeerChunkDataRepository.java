package com.paywithbytes.peer.db.repository;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.persistency.beans.PeerChunkData;

/**
 * We design the Repository with this interface because we might need to change
 * the SQLite implementation some time in the future. If we do that, this interface
 * will make the change easier. Even the junit tests are written for this interface
 * and not for the specific implementation.
 */

public interface PeerChunkDataRepository {

	public void insert(PeerChunkData peerFileMetadata) throws RepositoryException;

	public void update(PeerChunkData peerFileMetadata) throws RepositoryException;

	public PeerChunkData select(String uuidFragment) throws RepositoryException;

	public void delete(String uuidFragment) throws RepositoryException;

}