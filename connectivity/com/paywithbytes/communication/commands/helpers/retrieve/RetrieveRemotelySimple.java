package com.paywithbytes.communication.commands.helpers.retrieve;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.client.db.repository.UserFileMetadataRepository;
import com.paywithbytes.client.db.repository.UserFileMetadataSQLiteRepository;
import com.paywithbytes.common.encryption.SymmetricEncryptionHelper;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.jfec.file.wrapper.FileDecodeWrapper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class RetrieveRemotelySimple extends RetrieveRemotely {

	@Override
	public void retrieve(String uuid) throws CommunicationException, RepositoryException {
		UserFileMetadataRepository repo = new UserFileMetadataSQLiteRepository();

		UserFileMetadata metadata = repo.select(uuid);

		HttpClientHelper hch = new HttpClientHelper();
		Chunk chunkArray[] = new Chunk[metadata.getEncodingMetadata().getK()];

		int obtained = 0;
		for (int i = 0; i < metadata.getEncodingMetadata().getN(); i++) {
			RetrieveRemotelyChunk getChunk = new RetrieveRemotelyChunk();
			Chunk chunkItem = getChunk.retrieve(hch, metadata, metadata.getUserChunkMetadataList().get(i));

			if (chunkItem != null) {
				chunkArray[obtained] = chunkItem;
				obtained++;
				if (obtained >= metadata.getEncodingMetadata().getK())
					break;
			}
		}

		try {
			this.decodeAndWriteToFile(chunkArray, obtained, metadata);
		} catch (Exception e) {
			log.error("Unable to decode an create the new file.", e);
		}
		hch.close();

	}

	private void decodeAndWriteToFile(Chunk[] chunkArray, int obtained, UserFileMetadata metadata)
			throws Exception {

		if (obtained == metadata.getEncodingMetadata().getK()) {
			log.info("Going to decode the chunks");
			SymmetricEncryptionHelper.decryptChunks(chunkArray, metadata);

			FileDecodeWrapper decoder = new FileDecodeWrapper(chunkArray, metadata.getFileName(),
					metadata.getEncodingMetadata());
			decoder.decodeToFile();
		} else {
			log.error(String.format("Error, we could not decode the file because we only received [%d] chunks but"
					+ "we need it [%d] to be able to decode", obtained, metadata.getEncodingMetadata().getK()));
		}
	}

}
