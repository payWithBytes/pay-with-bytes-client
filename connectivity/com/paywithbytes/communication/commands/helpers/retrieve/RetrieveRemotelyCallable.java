package com.paywithbytes.communication.commands.helpers.retrieve;

import java.util.concurrent.Callable;

import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class RetrieveRemotelyCallable implements Callable<Chunk> {

	private UserFileMetadata metadata;
	private UserChunkMetadata chunkMetadata;
	private HttpClientHelper hch;

	public RetrieveRemotelyCallable(HttpClientHelper hch, UserFileMetadata metadata, UserChunkMetadata chunkMetadata) {
		this.metadata = metadata;
		this.chunkMetadata = chunkMetadata;
		this.hch = hch;
	}

	@Override
	public Chunk call() throws Exception {

		RetrieveRemotelyChunk getChunk = new RetrieveRemotelyChunk();
		try {
			return getChunk.retrieve(hch, metadata, chunkMetadata);
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
