package com.paywithbytes.communication.commands.helpers.retrieve;

import org.apache.log4j.Logger;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.commands.helpers.store.StoreRemotely;
import com.paywithbytes.communication.exceptions.CommunicationException;

public abstract class RetrieveRemotely {

	protected static final Logger log = LogHelper.getLogger(StoreRemotely.class);

	public abstract void retrieve(String uuid) throws CommunicationException, RepositoryException;
}
