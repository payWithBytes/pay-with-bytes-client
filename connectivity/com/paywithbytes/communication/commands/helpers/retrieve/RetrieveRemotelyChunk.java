package com.paywithbytes.communication.commands.helpers.retrieve;

import javax.servlet.http.HttpServletResponse;

import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.RetrieveRequest;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.PeerChunkData;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class RetrieveRemotelyChunk {

	private final org.apache.log4j.Logger log = LogHelper.getLogger(RetrieveRemotelyChunk.class);

	public Chunk retrieve(HttpClientHelper hch, UserFileMetadata metadata, UserChunkMetadata chunkMetadata)
			throws CommunicationException {

		// HttpClientHelper hch = new HttpClientHelper();
		Chunk retVal = null;
		Host host = JsonHelper.fromJson(chunkMetadata.getNodeStoringIt(), Host.class);

		RetrieveRequest request = new RetrieveRequest(chunkMetadata.getFragmentUUID(),
				metadata.getChallengeSolution());

		log.info("Request to send [" + request + "]");

		RequestResponse response = hch.doGet(JsonHelper.toJson(request, RetrieveRequest.class), host,
				AppConfig.httpURLRetrieve);

		if (response.getStatus() == HttpServletResponse.SC_FOUND) {
			retVal = JsonHelper.fromJson(response.getValue(), PeerChunkData.class).getChunk();
		}

		// hch.close();

		return retVal;

	}
}
