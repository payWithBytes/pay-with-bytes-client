package com.paywithbytes.communication.commands.helpers.retrieve;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.security.crypto.codec.Base64;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.client.db.repository.UserFileMetadataRepository;
import com.paywithbytes.client.db.repository.UserFileMetadataSQLiteRepository;
import com.paywithbytes.common.encryption.SymmetricEncryptionHelper;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.jfec.file.wrapper.FileDecodeWrapper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class RetrieveRemotelyMultipleThreads extends RetrieveRemotely {

	@Override
	public void retrieve(String uuid) throws CommunicationException, RepositoryException {
		UserFileMetadataRepository repo = new UserFileMetadataSQLiteRepository();

		UserFileMetadata metadata = repo.select(uuid);

		this.getChunksAndWriteFile(metadata);

	}

	private void getChunksAndWriteFile(UserFileMetadata metadata) throws CommunicationException {
		HttpClientHelper hch = new HttpClientHelper();
		// Start n threads
		int nThreads = metadata.getEncodingMetadata().getN();
		ExecutorService executor = Executors.newFixedThreadPool(nThreads);
		CompletionService<Chunk> ecs = new ExecutorCompletionService<Chunk>(executor);

		// Create N Callable solvers
		Collection<Callable<Chunk>> solvers = new ArrayList<Callable<Chunk>>();
		for (UserChunkMetadata userChunkMetadata : metadata.getUserChunkMetadataList()) {
			solvers.add(new RetrieveRemotelyCallable(hch, metadata, userChunkMetadata));
		}

		// Create the list of Future
		List<Future<Chunk>> futures = new ArrayList<Future<Chunk>>(metadata.getEncodingMetadata().getN());

		// We only need an array of K dimension
		Chunk[] chunkArray = new Chunk[metadata.getEncodingMetadata().getK()];

		// Get the first K threads that finishes.
		int obtained = 0;
		try {
			for (Callable<Chunk> s : solvers)
				futures.add(ecs.submit(s));

			// TODO: Fix this, for large files this approach is BAD, because we will
			// have unnecessary I/O of the (n-k) threads that we don't need.
			for (int i = 0; i < metadata.getEncodingMetadata().getN(); ++i) {
				try {
					Chunk r = ecs.take().get();
					if (r != null) {
						chunkArray[obtained] = r;
						obtained++;
						if (obtained >= metadata.getEncodingMetadata().getK()) {
							break;
						}
					} else {
						log.error("Thread failed (host down ?), going to continue with the next one.");
					}
				} catch (ExecutionException | InterruptedException ignore) {
					log.debug("Ignore this error", ignore);
				}
			}
		} finally {
			// Cancel the rest of the threads (n - k)
			log.info("Going to cancel the rest of threads.");
			for (Future<Chunk> f : futures) {
				f.cancel(true);
			}

		}

		executor.shutdown();

		log.info(String.format("Retrieve [%d] chunks!", obtained));
		hch.close();
		try {
			this.decodeAndWriteToFile(chunkArray, obtained, metadata);
		} catch (Exception e) {
			log.error("Error when decrypting chunks", e);
		}
	}

	private void decryptChunks(Chunk[] chunkArray, UserFileMetadata userFileMetadata) throws Exception {

		if (userFileMetadata.getType() == Types.Private) {

			SymmetricEncryptionHelper encryption = new SymmetricEncryptionHelper();

			for (Chunk chunk : chunkArray) {
				byte[] decodedBytes = Base64.decode(chunk.getData());
				byte[] decryptedBytes = encryption.decrypt(decodedBytes, userFileMetadata.getSymmetricKey(),
						userFileMetadata.getSymmetricKeyIV());

				chunk.setData(decryptedBytes);
			}
		}

	}

	private void decodeAndWriteToFile(Chunk[] chunkArray, int obtained, UserFileMetadata metadata)
			throws Exception {

		if (obtained == metadata.getEncodingMetadata().getK()) {
			log.info("Going to decode the chunks");
			this.decryptChunks(chunkArray, metadata);

			FileDecodeWrapper decoder = new FileDecodeWrapper(chunkArray, metadata.getFileName(),
					metadata.getEncodingMetadata());
			decoder.decodeToFile();
		} else {
			log.error(String.format("Error, we could not decode the file because we only received [%d] chunks but"
					+ "we need it [%d] to be able to decode", obtained, metadata.getEncodingMetadata().getK()));
		}
	}

}
