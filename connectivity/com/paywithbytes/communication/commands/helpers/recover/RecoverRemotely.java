package com.paywithbytes.communication.commands.helpers.recover;

import org.apache.log4j.Logger;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.exceptions.CommunicationException;

public abstract class RecoverRemotely {

	protected static final Logger log = LogHelper.getLogger(RecoverRemotely.class);

	public abstract void recover(String userId) throws CommunicationException, RepositoryException;
}