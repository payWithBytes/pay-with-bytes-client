package com.paywithbytes.communication.commands.helpers.recover;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.reflect.TypeToken;
import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.client.db.repository.UserFileMetadataRepository;
import com.paywithbytes.client.db.repository.UserFileMetadataSQLiteRepository;
import com.paywithbytes.common.encryption.PublicKeyEncryptionHelper;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class RecoverRemotelySimple extends RecoverRemotely {

	@Override
	public void recover(String userId) throws CommunicationException, RepositoryException {
		HttpClientHelper hch = new HttpClientHelper();

		try {

			Host server = JsonHelper.fromJson(AppConfig.serverHost, Host.class);

			RequestResponse response = hch.doGet(null, server, AppConfig.serverHttpURLRecovery + userId);

			if (response.getStatus() != HttpServletResponse.SC_OK) {
				log.error("An error has ocurred while trying to retrieve data from the server");
				return;
			}

			List<UserFileMetadataWrapper> userFileMetadataWrapperList = JsonHelper.fromJsonArray(response.getValue(),
					new TypeToken<List<UserFileMetadataWrapper>>() {
					});

			log.info(String.format("Retrieved [%d] rows from the Server", userFileMetadataWrapperList.size()));

			UserFileMetadataRepository repository = new UserFileMetadataSQLiteRepository();

			for (UserFileMetadataWrapper item : userFileMetadataWrapperList) {
				UserFileMetadata userFileMetadata = null;
				try {
					userFileMetadata = PublicKeyEncryptionHelper.decryptUserFileMetadata(item,
							AppConfig.userPrivateKey);
				} catch (Exception e) {
					log.error("Could NOT decrypt data!", e);
				}
				if (userFileMetadata != null)
					repository.insert(userFileMetadata);
			}
		} finally {
			hch.close();
		}
	}
}
