package com.paywithbytes.communication.commands.helpers.store;

import java.util.List;

import org.apache.log4j.Logger;

import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public abstract class StoreRemotely {

	protected static final Logger log = LogHelper.getLogger(StoreRemotely.class);

	public abstract void storeRemotely(UserFileMetadata userFileMetadata, Chunk[] fullChunks, List<Host> hostList)
			throws CommunicationException;
}
