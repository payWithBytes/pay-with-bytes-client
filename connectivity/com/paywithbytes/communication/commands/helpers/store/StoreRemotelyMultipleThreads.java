package com.paywithbytes.communication.commands.helpers.store;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.PeerChunkMetadata;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class StoreRemotelyMultipleThreads extends StoreRemotely {

	@Override
	public void storeRemotely(UserFileMetadata userFileMetadata, Chunk[] fullChunks, List<Host> hostList)
			throws CommunicationException {

		// Sanitize parameters
		if (fullChunks.length != hostList.size())
			throw new CommunicationException("The amount of chunks to send should be equal to the size of the hostList");

		HttpClientHelper hch = new HttpClientHelper();
		hch.getHttpClient().setDispatchIO(true);
		int nThreads = userFileMetadata.getEncodingMetadata().getN();

		hch.getHttpClient().setMaxConnectionsPerDestination(nThreads);

		ExecutorService executor = Executors.newFixedThreadPool(nThreads);

		// Create N Callable solvers
		Collection<Callable<UserChunkMetadata>> solvers = new ArrayList<Callable<UserChunkMetadata>>();
		String transactionId = UUID.randomUUID().toString();
		for (int i = 0; i < nThreads; i++) {
			Host host = hostList.get(i);
			PeerChunkMetadata peerChunkMetadata = new PeerChunkMetadata(userFileMetadata, fullChunks[i], i);

			solvers.add(new StoreRemotelyCallable(hch, transactionId, fullChunks[i], peerChunkMetadata,
					userFileMetadata, host));
		}

		// Call all of the N Callables objects, and retrieve the value return.
		List<Future<UserChunkMetadata>> futureList;
		try {
			futureList = executor.invokeAll(solvers);

			for (int i = 0; i < userFileMetadata.getEncodingMetadata().getN(); i++) {
				// This metadata indicate where the data has been stored.
				userFileMetadata.addUserChunkMetadataItem(futureList.get(i).get());
			}

		} catch (InterruptedException | ExecutionException e) {
			log.error("We failed to store the N chunks, we need to do a rollback here!", e);
		}

		// This will make the executor accept no new threads
		// and finish all existing threads in the queue
		executor.shutdown();

		try {
			// Wait until all threads are finish
			executor.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			log.error("Error when awaiting for termination ", e);
		}

		// TODO: Are we updating the metadata in the Server ? I don't think so, this only work with StoreRemotelySimple.
		log.info("Finished all threads");

		hch.close();

	}

}
