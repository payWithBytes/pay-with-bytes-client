package com.paywithbytes.communication.commands.helpers.store;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import com.paywithbytes.common.encryption.PublicKeyEncryptionHelper;
import com.paywithbytes.common.encryption.SymmetricEncryptionHelper;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.PeerChunkMetadata;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class StoreRemotelySimple extends StoreRemotely {

	private final HttpClientHelper hch;

	public StoreRemotelySimple(HttpClientHelper hch) {
		this.hch = hch;
	}

	@Override
	public void storeRemotely(UserFileMetadata userFileMetadata, Chunk[] fullChunks, List<Host> hostList)
			throws CommunicationException {

		if (userFileMetadata.getType() == Types.Private) {
			try {
				SymmetricEncryptionHelper.encryptChunks(fullChunks, userFileMetadata);
			} catch (Exception e) {
				log.error("Could not encrypt the Chunks", e);
			}
		}

		// Sanitize parameters
		if (fullChunks.length != hostList.size())
			throw new CommunicationException("The amount of chunks to send should be equal to the size of the hostList");

		String transactionId = UUID.randomUUID().toString();
		for (int i = 0; i < fullChunks.length; i++) {
			Host host = hostList.get(i);
			PeerChunkMetadata chunkMetadata = new PeerChunkMetadata(userFileMetadata, fullChunks[i], i);

			StoreRemotelyChunkMetadata.store(transactionId, host, chunkMetadata, fullChunks[i], userFileMetadata,
					this.hch);

		}

		this.updateMetadataInServer(this.hch, userFileMetadata);

		hch.close();
	}

	private void updateMetadataInServer(HttpClientHelper hch, UserFileMetadata userFileMetadata)
			throws CommunicationException {

		String userId = AppConfig.userUUID;
		Host server = JsonHelper.fromJson(AppConfig.serverHost, Host.class);

		log.info("Going to post to server " + server + "Url" + AppConfig.serverHttpURLBackup + userId);

		UserFileMetadataWrapper wrapper = null;
		try {
			wrapper = PublicKeyEncryptionHelper.encryptUserFileMetadata(userFileMetadata,
					AppConfig.userPublicKey);

		} catch (Exception e) {
			log.error("Error when trying to encrypt metadata", e);
			return;
		}

		RequestResponse response = hch.doPost(JsonHelper.toJson(wrapper, UserFileMetadataWrapper.class), server,
				AppConfig.serverHttpURLBackup + userId);

		if (response.getStatus() == HttpServletResponse.SC_CREATED) {
			log.info("Successfully stored in the server");
		} else {
			log.error("Error when storing metadata in the server");
		}

	}
}
