package com.paywithbytes.communication.commands.helpers.store;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.StoreRequest;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.PeerChunkData;
import com.paywithbytes.persistency.beans.PeerChunkMetadata;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class StoreRemotelyChunkMetadata {

	protected static final Logger log = LogHelper.getLogger(StoreRemotelyChunkMetadata.class);

	public static void store(String transactionId, Host host, PeerChunkMetadata chunkMetadata, Chunk chunk,
			UserFileMetadata userFileMetadata, HttpClientHelper hch) throws CommunicationException {

		PeerChunkData pcData = new PeerChunkData(chunkMetadata, chunk);
		RequestResponse response = hch.doPost(
				JsonHelper.toJson(new StoreRequest(transactionId, pcData), StoreRequest.class), host,
				AppConfig.httpURLStore);

		if (response.getStatus() == HttpServletResponse.SC_CREATED) {
			log.info(String.format("Successfully inserted chunk [%s] in host [%s]",
					chunkMetadata.getFragmentUUID(), host));

			userFileMetadata.getUserChunkMetadataList().add(
					new UserChunkMetadata(userFileMetadata.getUUID(), chunkMetadata
							.getFragmentUUID(), chunk
							.getData().length, JsonHelper.toJson(host, Host.class)));
		} else {
			log.error(String.format("Failed to insert chunk [%s] in host [%s]. Response Status [%d].",
					chunkMetadata.getFragmentUUID(), host, response.getStatus()));
		}
	}

	public static UserChunkMetadata storeReturnMetadata(String transactionId, Host host,
			PeerChunkMetadata chunkMetadata,
			Chunk chunk, String fileUUID, HttpClientHelper hch) throws CommunicationException {

		PeerChunkData pcData = new PeerChunkData(chunkMetadata, chunk);
		RequestResponse response = hch.doPost(
				JsonHelper.toJson(new StoreRequest(transactionId, pcData), StoreRequest.class), host,
				AppConfig.httpURLStore);

		if (response.getStatus() == HttpServletResponse.SC_CREATED) {
			log.info(String.format("Successfully inserted chunk [%s] in host [%s]",
					chunkMetadata.getFragmentUUID(), host));

			return new UserChunkMetadata(fileUUID, chunkMetadata
					.getFragmentUUID(), chunk
					.getData().length, JsonHelper.toJson(host, Host.class));
		} else {
			log.error(String.format("Failed to insert chunk [%s] in host [%s]. Response Status [%d].",
					chunkMetadata.getFragmentUUID(), host, response.getStatus()));
		}

		return null;
	}
}
