package com.paywithbytes.communication.commands.helpers.store;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.paywithbytes.common.encryption.SymmetricEncryptionHelper;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.jfec.wrapper.Chunk;
import com.paywithbytes.persistency.beans.PeerChunkMetadata;
import com.paywithbytes.persistency.beans.Types;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class StoreRemotelyCallable implements Callable<UserChunkMetadata> {

	protected static final Logger log = LogHelper.getLogger(StoreRemotelyCallable.class);

	private final HttpClientHelper hch;
	private final String transactionId;
	private final Chunk chunk;
	private final Host host;
	private final UserFileMetadata userFileMetadata;
	private final PeerChunkMetadata peerChunkMetadata;

	public StoreRemotelyCallable(HttpClientHelper hch, String transactionId,
			Chunk chunk, PeerChunkMetadata peerChunkMetadata, UserFileMetadata userFileMetadata, Host host) {
		this.chunk = chunk;
		this.host = host;
		this.transactionId = transactionId;
		this.userFileMetadata = userFileMetadata;
		this.hch = hch;
		this.peerChunkMetadata = peerChunkMetadata;
	}

	@Override
	public UserChunkMetadata call() throws Exception {
		try {

			if (userFileMetadata.getType() == Types.Private)
				SymmetricEncryptionHelper.encryptChunk(chunk, userFileMetadata);

			log.info("Going to store, I'm thread id [" + Thread.currentThread().getId() + "]");
			log.info("In server [" + host + "]. The chunk with id [" + peerChunkMetadata.getFragmentUUID());

			return StoreRemotelyChunkMetadata.storeReturnMetadata(transactionId, host, peerChunkMetadata, chunk,
					userFileMetadata.getUUID(), hch);

		} catch (Exception e) {
			log.error("Error when trying to send chunk to host [" + host + "]", e);
		}

		return null;
	}
}
