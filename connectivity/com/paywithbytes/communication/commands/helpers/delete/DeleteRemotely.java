package com.paywithbytes.communication.commands.helpers.delete;

import org.apache.log4j.Logger;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public abstract class DeleteRemotely {

	protected static final Logger log = LogHelper.getLogger(DeleteRemotely.class);

	public abstract void delete(UserFileMetadata userFileMetadata) throws CommunicationException, RepositoryException;
}