package com.paywithbytes.communication.commands.helpers.delete;

import javax.servlet.http.HttpServletResponse;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.encryption.PublicKeyEncryptionHelper;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.communication.beans.DeleteRequest;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.beans.UserFileMetadataWrapper;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.host.Host;
import com.paywithbytes.communication.http.helpers.HttpClientHelper;
import com.paywithbytes.persistency.beans.UserChunkMetadata;
import com.paywithbytes.persistency.beans.UserFileMetadata;

public class DeleteRemotelySimple extends DeleteRemotely {

	@Override
	public void delete(UserFileMetadata userFileMetadata) throws CommunicationException, RepositoryException {
		HttpClientHelper hch = new HttpClientHelper();

		int deletedCount = 0;
		for (int i = 0; i < userFileMetadata.getEncodingMetadata().getN(); i++) {
			UserChunkMetadata chunkMetadata = userFileMetadata.getUserChunkMetadataList().get(i);

			Host host = JsonHelper.fromJson(chunkMetadata.getNodeStoringIt(), Host.class);

			DeleteRequest request = new DeleteRequest(chunkMetadata.getFragmentUUID());
			RequestResponse response = hch.doDelete(JsonHelper.toJson(request, DeleteRequest.class), host,
					AppConfig.httpURLDelete);

			if (response.getStatus() == HttpServletResponse.SC_OK) {
				log.info(String.format("Deleted fragment [%s] from host [%s]", chunkMetadata.getFragmentUUID(), host));
				deletedCount++;
			} else {
				log.error(String.format("Error when deleting fragment [%s] from host [%s]. Response status [%d]",
						chunkMetadata.getFragmentUUID(), host, response.getStatus()));
			}
		}

		if (deletedCount != userFileMetadata.getEncodingMetadata().getN()) {
			log.error(String
					.format("An error has occured when deleting the fragments. We could only delete [%d] of the [%d] existing fragments",
							deletedCount, userFileMetadata.getEncodingMetadata().getN()));
		} else {
			log.info(String.format("Successfully deleted fragment [%s]", userFileMetadata.getUUID()));
		}

		updateMetadataInServer(hch, userFileMetadata);

		hch.close();
	}

	private void updateMetadataInServer(HttpClientHelper hch, UserFileMetadata userFileMetadata)
			throws CommunicationException {

		String userId = AppConfig.userUUID;
		Host server = JsonHelper.fromJson(AppConfig.serverHost, Host.class);

		log.info("Going to delete data from server " + server + "Url" + AppConfig.serverHttpURLDelete + userId);

		UserFileMetadataWrapper wrapper = null;
		try {
			// TODO: No need to encrypt and send the WHOLE userFileMetadataWrapper!! (fix this)
			wrapper = PublicKeyEncryptionHelper.encryptUserFileMetadata(userFileMetadata,
					AppConfig.userPublicKey);

		} catch (Exception e) {
			log.error("Error when trying to encrypt metadata", e);
			return;
		}

		RequestResponse response = hch.doDelete(JsonHelper.toJson(wrapper, UserFileMetadataWrapper.class), server,
				AppConfig.serverHttpURLDelete + userId);

		if (response.getStatus() == HttpServletResponse.SC_OK) {
			log.info("Successfully deleted in the server");
		} else {
			log.error("Error when deleting metadata in the server");
		}

	}

}
