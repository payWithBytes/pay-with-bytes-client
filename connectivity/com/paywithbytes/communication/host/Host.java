package com.paywithbytes.communication.host;

public class Host {

	public static void main(String[] args) {

		/*
		 * List<Host> hostList = new ArrayList<>();
		 * 
		 * hostList.add(new Host("192.168.2.1", 8090, "http"));
		 * hostList.add(new Host("192.168.2.2", 8090, "http"));
		 * hostList.add(new Host("192.168.2.3", 8090, "http"));
		 * 
		 * System.out.println(JsonHelper.toJson(hostList, List.class));
		 */

	}

	public Host(String ip, Integer port, String protocol) {
		this.setProtocol(protocol);
		this.setPort(port);
		this.setIp(ip);
	}

	private String protocol;
	private Integer port;
	private String ip;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		return String.format("IP:[%s], Port:[%d], Protocol:[%s]", this.ip, this.port, this.protocol);
	}

	public String getURI() {
		return this.getProtocol() + "://" + this.getIp() + ":" + this.getPort();
	}

}
