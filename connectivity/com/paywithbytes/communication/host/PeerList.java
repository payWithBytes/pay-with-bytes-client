package com.paywithbytes.communication.host;

import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.AppConfig;

public class PeerList {

	public static void main(String args[]) {

		List<Host> peerList = PeerList.getPeerList();

		System.out.println(peerList.size());

	}

	public static List<Host> getPeerList() {

		return JsonHelper.fromJsonArray(AppConfig.peerList, new TypeToken<List<Host>>() {
		});
	}
}
