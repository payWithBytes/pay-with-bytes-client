package com.paywithbytes.communication.http.server.servlet;

import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.persistency.beans.PeerChunkData;

public class ServletHelper {

	/**
	 * Build the response we are going to send to the caller.
	 * 
	 * @param data
	 * @return
	 */
	public static String buildResponse(PeerChunkData data, int statusCode, String messageDescription) {
		RequestResponse retVal = new RequestResponse(statusCode, messageDescription,
				data != null ? JsonHelper.toJson(data, PeerChunkData.class) : null);

		return JsonHelper.toJson(retVal, RequestResponse.class);
	}
}
