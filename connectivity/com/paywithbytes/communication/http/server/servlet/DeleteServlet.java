package com.paywithbytes.communication.http.server.servlet;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.communication.beans.DeleteRequest;
import com.paywithbytes.peer.db.repository.PeerChunkDataRepository;
import com.paywithbytes.peer.db.repository.PeerChunkDataSQLiteRepository;

public class DeleteServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static PeerChunkDataRepository repository;

	public DeleteServlet() {
		DeleteServlet.repository = new PeerChunkDataSQLiteRepository();
	}

	/**
	 * Parse the DELETE request sent by the caller.
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private DeleteRequest parseRequest(HttpServletRequest request) throws IOException {
		BufferedReader reader = request.getReader();

		StringBuilder builder = new StringBuilder();
		String aux = "";

		while ((aux = reader.readLine()) != null) {
			builder.append(aux);
		}

		String jsonValue = builder.toString();

		return JsonHelper.fromJson(jsonValue, DeleteRequest.class);
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		DeleteRequest deleteRequest = this.parseRequest(request);
		try {
			DeleteServlet.repository.delete(deleteRequest.getFragmentUUID());
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().write(
					ServletHelper.buildResponse(null, HttpServletResponse.SC_OK, "Item successfully deleted"));
		} catch (RepositoryException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_CONFLICT);
		}

	}
}
