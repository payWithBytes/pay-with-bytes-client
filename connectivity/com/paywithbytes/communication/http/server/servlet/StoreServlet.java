package com.paywithbytes.communication.http.server.servlet;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.communication.beans.StoreRequest;
import com.paywithbytes.peer.db.repository.PeerChunkDataRepository;
import com.paywithbytes.peer.db.repository.PeerChunkDataSQLiteRepository;

public class StoreServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static PeerChunkDataRepository repository;

	public StoreServlet() {
		StoreServlet.repository = new PeerChunkDataSQLiteRepository();
	}

	/**
	 * Parse the POST request sent by the caller.
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private StoreRequest parseRequest(HttpServletRequest request) throws IOException {
		BufferedReader reader = request.getReader();

		StringBuilder builder = new StringBuilder();
		String aux = "";

		while ((aux = reader.readLine()) != null) {
			builder.append(aux);
		}

		String jsonValue = builder.toString();

		return JsonHelper.fromJson(jsonValue, StoreRequest.class);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		StoreRequest storeRequest = this.parseRequest(request);
		try {
			StoreServlet.repository.insert(storeRequest.getPeerChunkData());
			response.setStatus(HttpServletResponse.SC_CREATED);
			response.getWriter().write(
					ServletHelper.buildResponse(null, HttpServletResponse.SC_CREATED, "Item successfully stored"));
		} catch (RepositoryException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_CONFLICT);
		}

	}

}