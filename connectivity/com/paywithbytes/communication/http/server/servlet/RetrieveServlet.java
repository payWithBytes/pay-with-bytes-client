package com.paywithbytes.communication.http.server.servlet;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.paywithbytes.client.db.exception.RepositoryException;
import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.beans.RetrieveRequest;
import com.paywithbytes.communication.http.helpers.HttpServerHelperSingleton;
import com.paywithbytes.peer.db.repository.PeerChunkDataRepository;
import com.paywithbytes.peer.db.repository.PeerChunkDataSQLiteRepository;
import com.paywithbytes.persistency.beans.PeerChunkData;

/**
 * 
 * Handles the /payWithBytes/retrieve content. We only implemented the GET method.
 * 
 */
public class RetrieveServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static PeerChunkDataRepository repository;
	private static Logger log = LogHelper.getLogger(HttpServerHelperSingleton.class);

	public RetrieveServlet() {
		repository = new PeerChunkDataSQLiteRepository();
	}

	/**
	 * Parse the request from the other client. The object type that the sender should send
	 * is RetrieveRequest.
	 * 
	 * @param request
	 * @return the object sent by the peer trying to retrieve content.
	 * @throws IOException
	 */
	private RetrieveRequest parseRequest(HttpServletRequest request) throws IOException {
		BufferedReader reader = request.getReader();

		StringBuilder builder = new StringBuilder();
		String aux = "";

		while ((aux = reader.readLine()) != null) {
			builder.append(aux);
		}

		String jsonValue = builder.toString();

		return JsonHelper.fromJson(jsonValue, RetrieveRequest.class);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		RetrieveRequest retrieveRequest = null;
		try {
			retrieveRequest = parseRequest(request);
			log.info("Retrieve request = [" + retrieveRequest + "]");
		} catch (IOException e) {
			response.getWriter()
					.write(
							ServletHelper.buildResponse(null, HttpServletResponse.SC_BAD_REQUEST,
									"Could not parse the Retrieve Request"));
			return;
		}

		response.setContentType("text/json");

		try {
			PeerChunkData data = repository.select(retrieveRequest.getFragmentUUID());

			if (data == null) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				response.getWriter()
						.write(ServletHelper.buildResponse(null, HttpServletResponse.SC_NOT_FOUND, "Item NOT found"));
				return;
			} else if (data.getPeerChunkMetadata().getChallengeSolution()
					.equals(retrieveRequest.getChallengeSolution().toString())) {
				// The challenge solution sent by the Peer should be the same he/she use to stored it.

				response.setStatus(HttpServletResponse.SC_OK);

				// As all the communication, we build a RequestResponse to let the caller know what
				// was the result of the request.
				response.getWriter().write(
						ServletHelper.buildResponse(data, HttpServletResponse.SC_FOUND, "Item Found"));

			} else {
				response.getWriter().write(
						ServletHelper.buildResponse(null, HttpServletResponse.SC_FORBIDDEN,
								"Challenge was not solved correctly."));
			}

		} catch (RepositoryException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().write(
					ServletHelper.buildResponse(null, HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
							"Internal Server Error"));
			log.error("doGet method error", e);
		}
	}

}