package com.paywithbytes.communication.http.helpers;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;

import com.paywithbytes.common.utils.AppConfig;
import com.paywithbytes.common.utils.LogHelper;
import com.paywithbytes.communication.http.server.servlet.DeleteServlet;
import com.paywithbytes.communication.http.server.servlet.RetrieveServlet;
import com.paywithbytes.communication.http.server.servlet.StoreServlet;

public class HttpServerHelperSingleton implements Runnable {

	private static Logger log = LogHelper.getLogger(HttpServerHelperSingleton.class);
	private static Server server;

	private static final HttpServerHelperSingleton instance;

	static {
		instance = new HttpServerHelperSingleton();
	}

	public static HttpServerHelperSingleton getInstance() {
		return instance;
	}

	private HttpServerHelperSingleton() {
		// ....
	}

	/**
	 * For test purposes only (?)
	 * 
	 * @param port
	 */
	public void runServer(int port) {
		HttpServerHelperSingleton.server = new Server(port);
		executeServer(HttpServerHelperSingleton.server);
	}

	public void runServerWithAppConfigPort() {
		Integer port = Integer.parseInt(AppConfig.httpServerPort);
		Server server = new Server(port);
		executeServer(server);
	}

	private void executeServer(Server server) {
		try {

			ServletHandler handler = new ServletHandler();
			server.setHandler(handler);

			handler.addServletWithMapping(StoreServlet.class, AppConfig.httpURLStore);
			handler.addServletWithMapping(RetrieveServlet.class, AppConfig.httpURLRetrieve);
			handler.addServletWithMapping(DeleteServlet.class, AppConfig.httpURLDelete);

			server.start();

			log.info(String.format("Going to start server at port [%d]", server.getURI().getPort()));

			server.join();

		} catch (Exception e) {
			log.error("Catch an exception when starting the server", e);
		}

	}

	/*
	 * public void close() throws Exception {
	 * HttpServerHelperSingleton.server.stop();
	 * }
	 */

	public static void main(String[] args) throws Exception {
		HttpServerHelperSingleton.getInstance().runServerWithAppConfigPort();
	}

	@Override
	public void run() {
		HttpServerHelperSingleton.getInstance().runServerWithAppConfigPort();
	}
}