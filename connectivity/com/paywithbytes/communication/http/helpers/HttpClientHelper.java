package com.paywithbytes.communication.http.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Response;
import org.eclipse.jetty.client.util.BytesContentProvider;
import org.eclipse.jetty.client.util.InputStreamResponseListener;
import org.eclipse.jetty.http.HttpMethod;

import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.communication.beans.RequestResponse;
import com.paywithbytes.communication.exceptions.CommunicationException;
import com.paywithbytes.communication.host.Host;

public class HttpClientHelper {

	private static final Logger log = Logger.getLogger(HttpClientHelper.class);

	private HttpClient httpClient;

	public HttpClient getHttpClient() {
		return this.httpClient;
	}

	public HttpClientHelper() throws CommunicationException {

		// Instantiate HttpClient
		this.httpClient = new HttpClient();
		try {

			httpClient.start();

			// Set to 10K.
			httpClient.setRequestBufferSize(10 * 1024);

		} catch (Exception e) {
			throw new CommunicationException("Error when trying to start http client", e);
		}
	}

	public void close() {
		try {
			this.httpClient.stop();
		} catch (Exception e) {
			log.error("Could not stop the http client", e);
		}
	}

	public RequestResponse doPostOld(String jsonValue, Host host, String url) throws CommunicationException {

		try {
			// OutputStreamContentProvider provider = new OutputStreamContentProvider();

			Request request = httpClient.POST(
					host.getURI() + url).timeout(240, TimeUnit.SECONDS).content(
					new BytesContentProvider(jsonValue.getBytes()), "text/json");

			log.debug("I'm thread id" + Thread.currentThread().getId() + "and I will POST data now");

			ContentResponse response = request.send();

			log.info("The status is " + response.getStatus());

			if (response.getStatus() == HttpServletResponse.SC_CREATED) {
				String responseValue = response.getContentAsString();
				log.debug("I'm thread id" + Thread.currentThread().getId()
						+ "and I have just received the answer from the POST ");
				return JsonHelper.fromJson(responseValue, RequestResponse.class);
			}

			throw new CommunicationException(String.format("Received an error with status [%d] from the server",
					response.getStatus()));

		} catch (Exception e) {
			log.error("Error when trying to use the http client to POST data", e);
			throw new CommunicationException("Error when trying to use the http client to POST data", e);
		}
	}

	public RequestResponse doPost(String jsonValue, Host host, String url) throws CommunicationException {

		try {

			Request request = httpClient.POST(
					host.getURI() + url).timeout(300, TimeUnit.SECONDS).content(
					new BytesContentProvider(jsonValue.getBytes()), "text/json");

			InputStreamResponseListener listener = new InputStreamResponseListener();

			request.send(listener);

			// Wait for the response headers to arrive
			Response response = listener.get(300, TimeUnit.SECONDS);

			log.debug("I'm thread id" + Thread.currentThread().getId() + "and I will POST data now");
			log.info("The status is " + response.getStatus());

			if (response.getStatus() == HttpServletResponse.SC_CREATED) {
				InputStream responseContent = listener.getInputStream();
				String responseValue = this.convertStreamToString(responseContent);
				// String responseValue = response.getContentAsString();
				log.debug("I'm thread id" + Thread.currentThread().getId()
						+ "and I have just received the answer from the POST ");
				return JsonHelper.fromJson(responseValue, RequestResponse.class);
			}

			throw new CommunicationException(String.format("Received an error with status [%d] from the server",
					response.getStatus()));

		} catch (Exception e) {
			log.error("Error when trying to use the http client to POST data", e);
			throw new CommunicationException("Error when trying to use the http client to POST data", e);
		}
	}

	/**
	 * Convert an InputStream to String
	 * Thanks S.O. guys: http://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	private String convertStreamToString(InputStream is) throws IOException {
		Scanner scan = null;
		try {
			scan = new Scanner(is).useDelimiter("\\A");
			return scan.hasNext() ? scan.next() : "";
		} finally {
			scan.close();
		}
	}

	public RequestResponse doGet(String jsonMessage, Host host, String url) throws CommunicationException {
		try {

			String uri = host.getURI() + url;
			InputStreamResponseListener listener = new InputStreamResponseListener();
			httpClient
					.newRequest(uri)
					.content(jsonMessage != null ? new BytesContentProvider(jsonMessage.getBytes()) : null, "text/json")
					.send(listener);

			// Wait for the response headers to arrive (Timeout in 240 seconds)
			Response response = listener.get(240, TimeUnit.SECONDS);

			log.info("The status is [" + response.getStatus() + "]");

			// Look at the response
			if (response.getStatus() == HttpServletResponse.SC_OK
					|| response.getStatus() == HttpServletResponse.SC_NOT_FOUND)
			{
				InputStream responseContent = listener.getInputStream();
				String jsonValue = this.convertStreamToString(responseContent);
				responseContent.close();
				log.debug("The size of the content in bytes is [" + jsonValue.length() + "]");
				return JsonHelper.fromJson(jsonValue, RequestResponse.class);
			}

			log.error(String.format("We received an error with status [%d] from the host [%s]", response.getStatus(),
					host));
			return null;
		} catch (Exception e) {
			log.error("Error when trying to use the http client to GET data", e);
			throw new CommunicationException("Error when trying to use the http client to GET data", e);
		}

	}

	public RequestResponse doDelete(String jsonMessage, Host host, String uriStr) throws CommunicationException {
		try {

			String uri = host.getURI() + uriStr;
			Request request = httpClient
					.newRequest(uri).method(HttpMethod.DELETE)
					.content(new BytesContentProvider(jsonMessage.getBytes()), "text/json");

			ContentResponse response = request.send();

			log.info("The status is " + response.getStatus());
			if (response.getStatus() == HttpServletResponse.SC_OK) {
				String jsonValue = response.getContentAsString();
				return JsonHelper.fromJson(jsonValue, RequestResponse.class);
			}

			log.error(String.format("We received an error with status [%d] from the host [%s]", response.getStatus(),
					host));
			return null;
		} catch (Exception e) {
			log.error("Error when trying to use the http client to DELETE data", e);
			throw new CommunicationException("Error when trying to use the http client to POST data", e);
		}
	}

}
