package com.paywithbytes.persistency.beans;

import com.paywithbytes.common.serialization.JsonHelper;
import com.paywithbytes.jfec.wrapper.Chunk;

/**
 * This bean contains the Peers' Metadata information of a given chunk.
 * 
 */
public class PeerChunkMetadata {

	// public static final String PUBLIC_TYPE = "Public";
	// public static final String PRIVATE_TYPE = "Private";

	public PeerChunkMetadata(UserFileMetadata userFileMetadata, Chunk chunk, int fragmentNumber) {
		this(userFileMetadata.getUUID() + "-" + fragmentNumber,
				chunk.getData().length,
				JsonHelper.toJson(userFileMetadata.getChallengeAssociated(), Challenge.class),
				Double.toString(userFileMetadata.getChallengeSolution()),
				userFileMetadata.getType());

	}

	public PeerChunkMetadata(String fragmentUUID, long chunkLength, String challengeAssociated,
			String challengeSolution, Types type) {
		this.setFragmentUUID(fragmentUUID);
		this.setChunkLength(chunkLength);
		this.setChallengeAssociated(challengeAssociated);
		this.setChallengeSolution(challengeSolution);
		this.setType(type);
	}

	private String fragmentUUID;
	private long chunkLength;
	private String challengeAssociated;
	private String challengeSolution;
	private Types type;
	private long timeStamp;

	public String getFragmentUUID() {
		return fragmentUUID;
	}

	public void setFragmentUUID(String fragmentUUID) {
		this.fragmentUUID = fragmentUUID;
	}

	public long getChunkLength() {
		return chunkLength;
	}

	public void setChunkLength(long chunkLength) {
		this.chunkLength = chunkLength;
	}

	public String getChallengeAssociated() {
		return challengeAssociated;
	}

	public void setChallengeAssociated(String challengeAssociated) {
		this.challengeAssociated = challengeAssociated;
	}

	public Types getType() {
		return type;
	}

	public void setType(Types type) {
		this.type = type;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	public boolean equals(Object o) {
		/*
		 * private String fragmentUUID;
		 * private long chunkLength;
		 * private String challengeAssociated;
		 * private String type;
		 * private long timeStamp;
		 */
		PeerChunkMetadata other = (PeerChunkMetadata) o;

		if (this.getFragmentUUID().equalsIgnoreCase(other.getFragmentUUID()) &&
				this.getChunkLength() == other.getChunkLength() &&
				this.getChallengeAssociated().equalsIgnoreCase(other.getChallengeAssociated()) &&
				this.getType().equals(other.getType()) &&
				this.getTimeStamp() == other.getTimeStamp()) {
			return true;
		}

		return false;
	}

	public String getChallengeSolution() {
		return challengeSolution;
	}

	public void setChallengeSolution(String challengeSolution) {
		this.challengeSolution = challengeSolution;
	}

}
