package com.paywithbytes.persistency.beans;

/**
 * These are the challenges that the clients will need to solve.
 * See the full description of the protocol for more information.
 * 
 */
public class Challenge {

	public Challenge(String firstTerm, String operator, String secondTerm) {
		this.setFirstTerm(firstTerm);
		this.setOperator(operator);
		this.setSecondTerm(secondTerm);
	}

	private String firstTerm;
	private String operator;
	private String secondTerm;

	public String getFirstTerm() {
		return firstTerm;
	}

	public void setFirstTerm(String firstTerm) {
		this.firstTerm = firstTerm;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getSecondTerm() {
		return secondTerm;
	}

	public void setSecondTerm(String secondTerm) {
		this.secondTerm = secondTerm;
	}

	@Override
	public String toString() {
		return this.getFirstTerm() + this.getOperator() + this.getSecondTerm();
	}

	@Override
	public boolean equals(Object o) {

		Challenge other = (Challenge) o;

		if (this.getFirstTerm().equals(other.getFirstTerm()) &&
				this.getOperator().equals(other.getOperator()) &&
				this.getSecondTerm().equals(other.getSecondTerm())) {
			return true;
		}

		return false;
	}

}
