package com.paywithbytes.persistency.beans;

/**
 * The types of the files (fragments and chunks)
 * 
 */
public enum Types {
	Public("Public"),
	Private("Private");

	private final String type;

	private Types(String s) {
		type = s;
	}

	/*
	 * public boolean equalsName(String otherName) {
	 * return (otherName == null) ? false : type.equals(otherName);
	 * }
	 */

	@Override
	public String toString() {
		return type;
	}

	/*
	 * public static void main(String args[]) {
	 * System.out.println(Types.Public);
	 * 
	 * Types myType = Types.valueOf("Public");
	 * 
	 * }
	 */

}
