package com.paywithbytes.persistency.beans;

import java.util.Arrays;

import com.paywithbytes.jfec.wrapper.Chunk;

/**
 * This class contains the Peers' information of an entire file. Furthermore, it contains
 * the information about the Chunks metadata.
 * 
 */
public class PeerChunkData {

	public PeerChunkData() {

	}

	public PeerChunkData(PeerChunkMetadata peerChunkMetadata, Chunk chunk) {
		this.setChunk(chunk);
		this.setPeerChunkMetadata(peerChunkMetadata);
	}

	private PeerChunkMetadata peerChunkMetadata;
	private Chunk chunk;

	public Chunk getChunk() {
		return chunk;
	}

	public void setChunk(Chunk chunk) {
		this.chunk = chunk;
	}

	public PeerChunkMetadata getPeerChunkMetadata() {
		return peerChunkMetadata;
	}

	public void setPeerChunkMetadata(PeerChunkMetadata peerChunkMetadata) {
		this.peerChunkMetadata = peerChunkMetadata;
	}

	@Override
	public boolean equals(Object o) {
		PeerChunkData other = (PeerChunkData) o;

		if (Arrays.equals(this.getChunk().getData(), other.getChunk().getData()) &&
				this.getChunk().getRepairIndex() == other.getChunk().getRepairIndex() &&
				this.getPeerChunkMetadata().equals(other.getPeerChunkMetadata())) {
			return true;
		}

		return false;
	}
}
