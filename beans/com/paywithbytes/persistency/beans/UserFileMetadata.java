package com.paywithbytes.persistency.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.paywithbytes.jfec.wrapper.EncodingMetadata;

/**
 * This class contains the Users' information of an entire file. Furthermore, it contains
 * the information about the Chunks metadata (in a List)
 * 
 */
public class UserFileMetadata {

	public UserFileMetadata() {
		this.setUserChunkMetadataList(new ArrayList<UserChunkMetadata>());
	}

	public UserFileMetadata(String UUID, String fileName, byte[] symmetricKey, byte[] symmetricKeyIV,
			Challenge challengeAssociated,
			Double challengeSolution,
			Types type, long timeStamp, EncodingMetadata encodingMetadata) {

		this.setUUID(UUID);
		this.setFileName(fileName);
		this.setSymmetricKey(symmetricKey);
		this.setSymmetricKeyIV(symmetricKeyIV);
		this.setChallengeAssociated(challengeAssociated);
		this.setChallengeSolution(challengeSolution);
		this.setType(type);
		this.setTimeStamp(timeStamp);
		this.setUserChunkMetadataList(new ArrayList<UserChunkMetadata>());
		this.setEncodingMetadata(encodingMetadata);
	}

	private String UUID;
	private String fileName;
	private byte[] symmetricKey;
	private byte[] symmetricKeyIV;
	private Challenge challengeAssociated;
	private Double challengeSolution;
	private Types type;
	private long timeStamp;
	private List<UserChunkMetadata> userChunkMetadataList;
	private EncodingMetadata encodingMetadata;

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String name) {
		this.fileName = name;
	}

	public byte[] getSymmetricKey() {
		return symmetricKey;
	}

	public void setSymmetricKey(byte[] symmetricKey) {
		this.symmetricKey = symmetricKey;
	}

	public List<UserChunkMetadata> getUserChunkMetadataList() {
		return userChunkMetadataList;
	}

	public void setUserChunkMetadataList(List<UserChunkMetadata> userChunkMetadataList) {
		this.userChunkMetadataList = userChunkMetadataList;
	}

	public void addUserChunkMetadataItem(UserChunkMetadata item) {
		this.getUserChunkMetadataList().add(item);
	}

	public Challenge getChallengeAssociated() {
		return challengeAssociated;
	}

	public void setChallengeAssociated(Challenge challengeAssociated) {
		this.challengeAssociated = challengeAssociated;
	}

	public Types getType() {
		return type;
	}

	public void setType(Types type) {
		this.type = type;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	public boolean equals(Object o) {
		UserFileMetadata other = (UserFileMetadata) o;
		try {

			if (this.getUUID().equalsIgnoreCase(other.getUUID())
					&&
					this.getFileName().equals(other.getFileName())
					&&
					Arrays.equals(this.getSymmetricKey(), other.getSymmetricKey())
					&&
					this.getChallengeAssociated().toString()
							.equalsIgnoreCase(other.getChallengeAssociated().toString()) &&
					this.getType().equals(other.getType()) &&
					this.getEncodingMetadata().equals(other.getEncodingMetadata()) &&
					this.getTimeStamp() == other.getTimeStamp()) {

				if (this.getUserChunkMetadataList().size() == other.getUserChunkMetadataList().size() &&
						this.getUserChunkMetadataList().equals(other.getUserChunkMetadataList())) {
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

	public EncodingMetadata getEncodingMetadata() {
		return encodingMetadata;
	}

	public void setEncodingMetadata(EncodingMetadata encodingMetadata) {
		this.encodingMetadata = encodingMetadata;
	}

	public Double getChallengeSolution() {
		return challengeSolution;
	}

	public void setChallengeSolution(Double challengeSolution) {
		this.challengeSolution = challengeSolution;
	}

	public byte[] getSymmetricKeyIV() {
		return symmetricKeyIV;
	}

	public void setSymmetricKeyIV(byte[] symmetricKeyIV) {
		this.symmetricKeyIV = symmetricKeyIV;
	}
}
