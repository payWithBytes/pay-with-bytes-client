package com.paywithbytes.persistency.beans;

/**
 * This bean contains the Users' Metadata information of a given chunk.
 * 
 */
public class UserChunkMetadata {

	public UserChunkMetadata(String parentUUID, String fragmentUUID, long length, String nodeStoringIt) {
		this.setParentUUID(parentUUID);
		this.setFragmentUUID(fragmentUUID);
		this.setLength(length);
		this.setNodeStoringIt(nodeStoringIt);
	}

	private String parentUUID;
	private String fragmentUUID;
	private long length;
	private String nodeStoringIt;

	public String getParentUUID() {
		return parentUUID;
	}

	public void setParentUUID(String parentUUID) {
		this.parentUUID = parentUUID;
	}

	public String getFragmentUUID() {
		return fragmentUUID;
	}

	public void setFragmentUUID(String fragmentUUID) {
		this.fragmentUUID = fragmentUUID;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public String getNodeStoringIt() {
		return nodeStoringIt;
	}

	public void setNodeStoringIt(String nodeStoringIt) {
		this.nodeStoringIt = nodeStoringIt;
	}

	@Override
	public boolean equals(Object o) {

		UserChunkMetadata other = (UserChunkMetadata) o;

		if (this.getParentUUID().equalsIgnoreCase(other.getParentUUID()) &&
				this.getFragmentUUID().equalsIgnoreCase(other.getFragmentUUID()) &&
				this.getLength() == other.getLength() &&
				this.getNodeStoringIt().equalsIgnoreCase(other.getNodeStoringIt()))
			return true;

		return false;
	}
}
