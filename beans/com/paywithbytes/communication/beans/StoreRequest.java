package com.paywithbytes.communication.beans;

import com.paywithbytes.persistency.beans.PeerChunkData;

/**
 * 
 *
 */
public class StoreRequest {

	public StoreRequest() {

	}

	public StoreRequest(String transactionId, PeerChunkData peerChunkData) {
		this.setTransactionID(transactionID);
		this.setPeerChunkData(peerChunkData);
	}

	private String transactionID;
	private PeerChunkData peerChunkData;

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public PeerChunkData getPeerChunkData() {
		return peerChunkData;
	}

	public void setPeerChunkData(PeerChunkData peerChunkData) {
		this.peerChunkData = peerChunkData;
	}

}
