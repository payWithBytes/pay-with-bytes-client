package com.paywithbytes.communication.beans;

public class RequestResponse {

	public RequestResponse(int status, String description, String value) {
		this.setStatus(status);
		this.setDescription(description);
		this.setValue(value);
	}

	private int status;
	private String description;
	private String value;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
