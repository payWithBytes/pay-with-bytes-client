package com.paywithbytes.communication.beans;

public class DeleteRequest {

	private final String fragmentUUID;

	public DeleteRequest(String fragmentUUID) {
		this.fragmentUUID = fragmentUUID;
	}

	public String getFragmentUUID() {
		return fragmentUUID;
	}
}
