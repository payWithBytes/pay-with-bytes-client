package com.paywithbytes.communication.beans;

public class RetrieveRequest {

	private final String fragmentUUID;
	private final Double challengeSolution;

	public RetrieveRequest(String fragmentUUID, Double challengeSolution) {
		this.fragmentUUID = fragmentUUID;
		this.challengeSolution = challengeSolution;
	}

	public String getFragmentUUID() {
		return fragmentUUID;
	}

	public Double getChallengeSolution() {
		return challengeSolution;
	}

	@Override
	public String toString() {
		return String.format("FragmentId [%s] : ChallengeSolution : [" + challengeSolution + "]", fragmentUUID);
	}
}
