package com.paywithbytes.communication.beans;

public class UserFileMetadataWrapper {

	private byte[] userFileMetadata;
	private byte[] encryptedSymmetricKey;
	private byte[] iv;
	private String fileIdDigest;

	public UserFileMetadataWrapper() {

	}

	public UserFileMetadataWrapper(byte[] userFileMetadata, byte[] encryptedSymmetricKey, byte[] iv, String fileIdDigest) {
		this.setEncryptedSymmetricKey(encryptedSymmetricKey);
		this.setUserFileMetadata(userFileMetadata);
		this.setIv(iv);
		this.setFileIdDigest(fileIdDigest);
	}

	public byte[] getUserFileMetadata() {
		return userFileMetadata;
	}

	public void setUserFileMetadata(byte[] userFileMetadata) {
		this.userFileMetadata = userFileMetadata;
	}

	public byte[] getEncryptedSymmetricKey() {
		return encryptedSymmetricKey;
	}

	public void setEncryptedSymmetricKey(byte[] encryptedSymmetricKey) {
		this.encryptedSymmetricKey = encryptedSymmetricKey;
	}

	public byte[] getIv() {
		return iv;
	}

	public void setIv(byte[] iv) {
		this.iv = iv;
	}

	public String getFileIdDigest() {
		return fileIdDigest;
	}

	public void setFileIdDigest(String fileIdDigest) {
		this.fileIdDigest = fileIdDigest;
	}

}
